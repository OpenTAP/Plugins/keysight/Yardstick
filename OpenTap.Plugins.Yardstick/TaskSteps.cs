﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using OpenTap.Plugins.Yardstick.Objects;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.Yardstick
{
    [Display("List Tasks", Groups: new string[] { "Yardstick", "V2 API", "Task" }, Description: "List Available Yardstick Tasks")]
    public class ListTasksStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "The list of tasks that are available in Yardstick.", Group: "Output")]
        [Output]
        public List<V2Taskinfo> Tasks { get; set; }

        #endregion

        public ListTasksStep()
        {
            Name = "List Tasks";
        }

        public override void Run()
        {
            List<V2Taskinfo> tasks = Yardstick.V2GetTaskList();

            if (tasks != null)
            {
                Tasks = tasks;
                UpgradeVerdict(Verdict.Pass);
            }
            else
                UpgradeVerdict(Verdict.Fail);
        }
    }
    
    [Display("Update Environment", Groups: new string[] { "Yardstick", "V2 API", "Task" }, Description: "Add an environment to a task")]
    public class AddEnvironmentStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Task Id From Test Step", Group: "Settings")]
        public bool TaskIdFromStep { get; set; } = true;

        [Display(Name: "Task Id", Group: "Settings")]
        [EnabledIf("TaskIdFromStep", true, HideIfDisabled = true)]
        public Input<string> TaskIdInput { get; set; }

        [Display(Name: "Task Id", Group: "Settings")]
        [EnabledIf("TaskIdFromStep", false, HideIfDisabled = true)]
        public string TaskIdString { get; set; }


        [Display(Name: "Environment Id", Group: "Settings")]
        public Input<string> EnvironmentId { get; set; }

        [Display(Name: "Added", Group: "Ouput")]
        [Output]
        public bool Added { get; protected set; }
        #endregion

        public AddEnvironmentStep()
        {
            Name = "Update task environment";

            TaskIdInput = new Input<string>();
            EnvironmentId = new Input<string>();

            Rules.Add(() => TaskIdFromStep == true && TaskIdInput.Step != null, "A Task Id is required.", "TaskIdInput");
            Rules.Add(() => !TaskIdFromStep && !string.IsNullOrEmpty(TaskIdString), "A Task Id is required.", "TaskIdString");
            Rules.Add(() => EnvironmentId.Step != null, "An Environment Id Id is required.", "EnvironmentId");
        }

        public override void Run()
        {
            var taskId = TaskIdFromStep ? TaskIdInput.Value : TaskIdString;
            Added = Yardstick.AddEnvironmentToTask(taskId, EnvironmentId.Value);
            if(Added == true)
                UpgradeVerdict(Verdict.Pass);
            else
                UpgradeVerdict(Verdict.Fail);
        }
    }

    [Display("Create Task (Test Case From File)", Groups: new string[] { "Yardstick", "V2 API", "Task" },  Description: "Create task with test case from file")]
    public class CreateTaskFromTestCaseFile : CreateTaskStep
    {
        private string fileContentAsBase64 = null;
        #region Settings

        [Display(Name: "Test Case Name", Group: "Settings", Description: "The name of the test case to upload.")]
        public string TestCaseName { get; set; }

        [FilePath()]
        [Display(Name: "Test case file", Description: "Test case file url.", Group: "Settings")]
        public string FileBase64
        {
            get
            {
                return fileContentAsBase64;
            }
            set
            {
                bool fallback = true;
                FileInfo fi = null;
                try
                {
                    fi = new FileInfo(value);
                    if (fi.Exists)
                    {
                        string fileContent = File.ReadAllText(value);
                        fileContentAsBase64 = Base64Encoding.EncodeBase64(fileContent);
                        fallback = false;
                    }
                }
                catch (System.Exception ex)
                {
                    Log.Error(ex.Message);
                }

                if (fallback)
                {
                    if (Base64Encoding.IsBase64(value))
                    {
                        fileContentAsBase64 = value;
                    }
                    else
                    {
                        fileContentAsBase64 = null;
                    }
                }
            }
        }

        #endregion

        public CreateTaskFromTestCaseFile()
        {
            Name = "Create task with test case from file";

            Rules.Add(() => !string.IsNullOrEmpty(TestCaseName), "A test case name is required.", "TestCaseName");
            Rules.Add(() => !string.IsNullOrEmpty(FileBase64), "A test case file is required.", "FileBase64");
        }

        public override void Run()
        {
            // Read Test case file content
            byte[] content = Encoding.UTF8.GetBytes(Base64Encoding.DecodeBase64(fileContentAsBase64));
            Debug.Assert(content != null);

            // Read test case content
            string strTestCaseContent = PrintExceptions<string>(() => Encoding.UTF8.GetString(content, 0, content.Length));

            // Create task and add test case
            CreateTaskWithTestCase(TestCaseName, strTestCaseContent);
        }
    }

    [Display("Create Task (Test Case From DB)", Groups: new string[] { "Yardstick", "V2 API", "Task" }, Description: "Create task with test case from database")]
    public class CreateTaskFromDBTestCase : CreateTaskStep, ISuggestTestCaseStep //SuggestTestCasesStep
    {
        public IEnumerable<string> TCNames { get; protected set; }

        #region Settings

        [Display(Name: "Test Case Name", Group: "Settings")]
        [SuggestedValues("TCNames")]
        public string TestCaseName { get; set; }

        #endregion

        protected void UpdateTestCases()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() => IgnoreExceptions<List<Objects.TestCase>>(() => Yardstick.V2GetTestCaseList(false)))
            .ContinueWith(result =>
            {
                if (result.Result == null)
                    return;

                TCNames = result.Result.Select((testCase) => testCase.Name);
                this.OnPropertyChanged("TCNames");
            }, TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        public CreateTaskFromDBTestCase()
        {
            Name = "Create task with test case from database";

            UpdateTestCases();

            Rules.Add(() => !string.IsNullOrEmpty(TestCaseName), "A Test Case Name is required.", "TestCaseName");
        }

        public override void Run()
        {
            // Read Test case file content
            V2GetTestCaseResult testCaseResult = PrintExceptions<V2GetTestCaseResult>(() => Yardstick.GetTestCase(TestCaseName));
            Debug.Assert(testCaseResult != null);

            // Read test case content
            string strTestCaseContent = testCaseResult.TestCaseContent;
            Debug.Assert(strTestCaseContent != null && strTestCaseContent.Length > 0);

            // Create task and add test case
            CreateTaskWithTestCase(TestCaseName, strTestCaseContent);
        }
    }

    [Display("Create Test (Suite From File)", Groups: new string[] { "Yardstick", "V2 API", "Task" }, Description: "Create task with test suite from file")]
    public class CreateTaskFromTestSuiteFile : CreateTaskStep
    {
        private string fileContentAsBase64 = null;
        #region Settings

        [Display(Name: "Test Suite Name", Group: "Settings", Description: "The name of the test suite to upload.")]
        public string TestSuiteName { get; set; }

        [FilePath()]
        [Display(Name: "Test Suite file", Description: "Test suite file url.", Group: "Settings")]
        public string FileBase64
        {
            get
            {
                return fileContentAsBase64;
            }
            set
            {
                bool fallback = true;
                FileInfo fi = null;
                try
                {
                    fi = new FileInfo(value);
                    if (fi.Exists)
                    {
                        string fileContent = File.ReadAllText(value);
                        fileContentAsBase64 = Base64Encoding.EncodeBase64(fileContent);
                        fallback = false;
                    }
                }
                catch (System.Exception ex)
                {
                    Log.Error(ex.Message);
                }

                if (fallback)
                {
                    if (Base64Encoding.IsBase64(value))
                    {
                        fileContentAsBase64 = value;
                    }
                    else
                    {
                        fileContentAsBase64 = null;
                    }
                }
            }
        }

        #endregion

        public CreateTaskFromTestSuiteFile()
        {
            Name = "Create task with test suite from file";

            Rules.Add(() => !string.IsNullOrEmpty(TestSuiteName), "A test suite name is required.", "TestSuiteName");
            Rules.Add(() => !string.IsNullOrEmpty(FileBase64), "A test suite file is required.", "FileBase64");
        }

        public override void Run()
        {
            // Read Test case file content
            byte[] content = Encoding.UTF8.GetBytes(Base64Encoding.DecodeBase64(fileContentAsBase64));
            Debug.Assert(content != null);

            // Read test case content
            string strTestSuiteContent = PrintExceptions<string>(() => Encoding.UTF8.GetString(content, 0, content.Length));

            // Create task and add test case
            CreateTaskWithTestSuite(TestSuiteName, strTestSuiteContent);

            //string strTestSuiteContent = Encoding.UTF8.GetString(testSuiteContent, 0, testSuiteContent.Length);

            //Added = Yardstick.AddTestSuiteToTask(TaskId.Value, TestSuiteName, strTestSuiteContent);
            //if (Added == true)
            //    UpgradeVerdict(Verdict.Pass);
            //else
            //    UpgradeVerdict(Verdict.Fail);
        }
    }

    [Display("Create Task (Suite From DB)", Groups: new string[] { "Yardstick", "V2 API", "Task" }, Description: "Create task with test suite from database")]
    public class CreateTaskFromDBTestSuite : CreateTaskStep, ISuggestTestSuiteStep
    {
        public IEnumerable<string> TSNames { get; protected set; }

        #region Settings

        [Display(Name: "Test Suite Name", Group: "Settings")]
        [SuggestedValues("TSNames")]
        public string TestSuiteName { get; set; }

        #endregion

        protected void UpdateTestSuites()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() => IgnoreExceptions<List<string>>(() => Yardstick.GetTestSuiteList(false)))
            .ContinueWith(result =>
            {
                if (result.Result == null)
                    return;

                TSNames = result.Result.Select((fileName) =>
                {
                    int index = fileName.LastIndexOf('.');
                    return fileName.Substring(0, index);
                });
                this.OnPropertyChanged("TSNames");
            }, TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        public CreateTaskFromDBTestSuite()
        {
            Name = "Create task with test suite from database";

            UpdateTestSuites();

            Rules.Add(() => !string.IsNullOrEmpty(TestSuiteName), "A Test Suite Name is required.", "TestSuiteName");
        }

        public override void Run()
        {
            // Read Test Suite file content
            V2GetTestSuiteResult testSuiteResult = PrintExceptions<V2GetTestSuiteResult>(() => Yardstick.GetTestSuite(TestSuiteName));
            Debug.Assert(testSuiteResult != null);

            // Read test Suite content
            string strTestSuiteContent = testSuiteResult.TestSuiteContent;
            Debug.Assert(strTestSuiteContent != null && strTestSuiteContent.Length > 0);

            // Create task and add test case
            CreateTaskWithTestSuite(TestSuiteName, strTestSuiteContent);

            //bool success = false;
            //V2GetTestSuiteResult testSuiteResult = Yardstick.GetTestSuite(TestSuiteName);

            //if (testSuiteResult != null)
            //{
            //    string testSuiteContent = testSuiteResult.TestSuiteContent;

            //    success = Yardstick.AddTestSuiteToTask(TaskId.Value, TestSuiteName, testSuiteContent);
            //}
            //else
            //{
            //    Log.Info($"The error could be triggered if the test suite {TestSuiteName} doesn't exist.");
            //}

            //if (success)
            //{
            //    Added = true;
            //    UpgradeVerdict(Verdict.Pass);
            //}
            //else
            //{
            //    UpgradeVerdict(Verdict.Fail);
            //}
        }
    }

    //[Display("Add Test Suite", Groups: new string[] { "Yardstick", "V2 API", "Task" }, Description: "Add a test suite to a task")]
    //public class AddTestSuite : YardstickTestStep
    //{
    //    #region Settings

    //    [Display(Name: "Task Id", Group: "Settings")]
    //    public Input<string> TaskId { get; set; }

    //    [Display(Name: "Test Suite Name", Group: "Settings")]
    //    public string TestSuiteName { get; set; }

    //    [Display(Name: "Test Suite Content", Group: "Settings")]
    //    public string TestSuiteContent { get; set; }

    //    [Display(Name: "Added", Group: "Ouput")]
    //    [Output]
    //    public bool Added { get; protected set; }
    //    #endregion

    //    public AddTestSuite()
    //    {
    //        Name = "Add a test suite to a task";

    //        TaskId = new Input<string>();

    //        Rules.Add(() => TaskId.Step != null, "A Task Id is required.", "TaskId");
    //        Rules.Add(() => !string.IsNullOrEmpty(TestSuiteName), "A test suite name is required.", "TestSuiteName");
    //        Rules.Add(() => !string.IsNullOrEmpty(TestSuiteContent), "Test suite content is required.", "TestSuiteContent");
    //    }

    //    public override void Run()
    //    {
    //        Added = Yardstick.AddTestSuiteToTask(TaskId.Value, TestSuiteName, TestSuiteContent);
    //        if (Added == true)
    //            UpgradeVerdict(Verdict.Pass);
    //        else
    //            UpgradeVerdict(Verdict.Fail);
    //    }
    //}

    //[Display("Create Task", Groups: new string[] { "Yardstick", "V2 API", "Task" }, Description: "Create Yardstick Task")]
    //public class CreateTaskStep : YardstickTestStep
    //{
    //    #region Settings

    //    [Display(Name: "Task name", Group: "Settings")]
    //    public string TaskName { get; set; }

    //    [Display(Name: "Project id", Group: "Settings")]
    //    public Input<string> ProjectId { get; set; }

    //    [Display(Name: "Environment id", Group: "Settings")]
    //    public Input<string> EnvironmentId { get; set; }

    //    [Output]
    //    [Display(Name: "Task id", Group: "Output")]
    //    public string TaskId { get; set; }

    //    #endregion

    //    public CreateTaskStep()
    //    {
    //        Name = "Create Task";

    //        ProjectId = new Input<string>();
    //        EnvironmentId = new Input<string>();

    //        Rules.Add(() => !string.IsNullOrEmpty(TaskName), "Task Name is missing!", "TaskName");
    //        Rules.Add(() => ProjectId.Step != null, "Project Id is required.", "ProjectId");
    //        Rules.Add(() => EnvironmentId.Step != null, "Project Id is required.", "EnvironmentId");
    //    }

    //    public override void Run()
    //    {
    //        TaskId = Yardstick.CreateTask(TaskName, ProjectId.Value);
    //        UpgradeVerdict(Verdict.Pass);
    //    }
    //}

    [Display("Get Task Info", Groups: new string[] { "Yardstick", "V2 API", "Task" }, Description: "Get task information")]
    public class GetTaskInfo : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Task Id", Group: "Settings")]
        public Input<string> TaskId { get; set; }

        [Output]
        [Display(Name: "Task", Group: "Output", Description: "Information about the task")]
        public string Task { get; set; }

        #endregion

        public GetTaskInfo()
        {
            Name = "Get task information";
            TaskId = new Input<string>();

            Rules.Add(() => TaskId.Step != null, "A Task Id is required.", "TaskId");
        }

        public override void Run()
        {
            V2TaskInfoResult taskInfoResult = Yardstick.GetTaskInfo(TaskId.Value);
            if (taskInfoResult != null)
            {
                Task = YardstickTestStep.SerializeObject(taskInfoResult.Task);
                OnPropertyChanged("Task");
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }

    [Display("Delete Task", Groups: new string[] { "Yardstick", "V2 API", "Task" }, Description: "Delete a task")]
    public class DeleteTask : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Task Id", Group: "Settings")]
        public Input<string> TaskId { get; set; }

        #endregion

        public DeleteTask()
        {
            Name = "Delete task";

            TaskId = new Input<string>();

            Rules.Add(() => TaskId.Step != null, "A Task Id is required.", "TaskId");
        }

        public override void Run()
        {
            bool result = Yardstick.DeleteTask(TaskId.Value);
            if (result)
                UpgradeVerdict(Verdict.Pass);
            else
                UpgradeVerdict(Verdict.Fail);
        }
    }

    [Display("Run Task", Groups: new string[] { "Yardstick", "V2 API", "Task" }, Description: "Run Yardstick Task")]
    public class RunTaskStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Task id", Group: "Settings")]
        public Input<string> TaskId { get; set; }

        [Output]
        [Display(Name: "Status", Group: "Output", Description: "0(running), 1(finished) and 2(failed)")]
        public int Status { get; set; }

        #endregion

        public RunTaskStep()
        {
            Name = "Run Task";

            TaskId = new Input<string>();

            Rules.Add(() => TaskId.Step != null, "Task Id is required.", "TaskId");
        }

        public override void Run()
        {
            Tuple<string, int> result = Yardstick.RunTask(TaskId.Value);
            if (result != null)
            {
                //string taskId = result.Item1;
                Status = result.Item2;

                if (Status == 1)
                {
                    UpgradeVerdict(Verdict.Pass);
                }
                else if (Status == -1 || Status == 2)
                {
                    UpgradeVerdict(Verdict.Fail);
                }
                else
                {
                    Log.Info($"Unexpected status code {Status}");
                    UpgradeVerdict(Verdict.Inconclusive);
                }
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }
}

