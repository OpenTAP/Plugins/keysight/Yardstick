﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using OpenTap.Plugins.Yardstick.Objects;
using System;
using System.Collections.Generic;

namespace OpenTap.Plugins.Yardstick
{
    [Display("Create Project", Groups: new string[] { "Yardstick", "V2 API", "Project" }, Description: "Create Yardstick Project")]
    public class CreateProjectStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Project name", Group: "Settings")]
        public String ProjectName { get; set; }

        [Output]
        [Display(Name: "Project id", Group: "Output")]
        public String ProjectId { get; private set; }

        #endregion

        public CreateProjectStep()
        {
            Name = "Create Project";
            Rules.Add(() => !string.IsNullOrEmpty(ProjectName), "Project Name is missing!", "ProjectName");
        }

        public override void Run()
        {
            ProjectId = Yardstick.CreateProject(ProjectName);
            UpgradeVerdict(Verdict.Pass);
        }
    }

    [Display("Delete Project", Groups: new string[] { "Yardstick", "V2 API", "Project" }, Description: "Delete Project")]
    public class DeleteProjectStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Project Id", Group: "Settings")]
        public Input<string> ProjectId { get; set; }

        [Display(Name: "Project Deleted", Group: "Output")]
        [Output]
        public bool Deleted { get; set; }

        #endregion

        public DeleteProjectStep()
        {
            Name = "Delete Project";

            ProjectId = new Input<string>();

            Rules.Add(() => ProjectId.Step != null, "Project Id is missing!", "ProjectId");
        }

        public override void Run()
        {
            Deleted = Yardstick.DeleteProject(ProjectId.Value);
            if(Deleted == true)
                UpgradeVerdict(Verdict.Pass);
            else
                UpgradeVerdict(Verdict.Fail);
        }
    }
    
    [Display("Get Projects", Groups: new string[] { "Yardstick", "V2 API", "Project" }, Description: "List Available Yardstick Projects")]
    public class GetProjectsStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "The list of projects that are available in Yardstick.", Group: "Output")]
        [Output]
        public List<Project> Projects { get; set; }

        #endregion

        public GetProjectsStep()
        {
            Name = "Get Projects";
        }

        public override void Run()
        {
            List<Project> projects = Yardstick.GetProjects();
            if (projects != null)
            {
                Projects = projects;
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }
    
    [Display("Get Project Info", Groups: new string[] { "Yardstick", "V2 API", "Project" }, Description: "Get Project Info")]
    public class GetProjectInfoStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Project Id", Group: "Settings")]
        public Input<string> ProjectId { get; set; }

        [Display(Name: "Project Info.", Group: "Output")]
        [Output]
        public Project Project { get; set; }

        #endregion

        public GetProjectInfoStep()
        {
            Name = "Get Project Info";

            ProjectId = new Input<string>();

            Rules.Add(() => ProjectId.Step != null, "A project id is required.", "ProjectId");
        }

        public override void Run()
        {
            var project = Yardstick.GetProjectInfo(ProjectId.Value);

            if (project != null)
            {
                Project = project;
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }
}
