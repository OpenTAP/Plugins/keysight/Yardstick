﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using OpenTap.Plugins.Yardstick.Instruments;
using OpenTap.Plugins.Yardstick.Objects;
using System;
using System.Collections.Generic;

namespace OpenTap.Plugins.Yardstick
{

    [Display("Get Task Result", Groups: new string[] { "Yardstick", "V1 API" }, Description: "Get Yardstick Task Result")]
    public class GetTaskResultStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Task Id", Group: "Settings")]
        public Input<string> TaskId { get; set; }

        [Display(Name: "List of test case results as string", Group: "Output")]
        [Output]
        public string TestCaseResults { get; set; }

        #endregion

        public GetTaskResultStep()
        {
            Name = "Get Result";
            TaskId = new Input<string>();
            Rules.Add(() => !string.IsNullOrEmpty(TaskId.ToString()), "A Guid is required.", "TaskId");
        }

        [Display("Result Name")]
        class Result
        {
            public string TaskResult { get; set; }
        }

        public override void Run()
        {
            string results = Yardstick.GetResult(TaskId.Value);
            //Results.Publish(new Result() { TaskResult = results });
            if (results != null)
            {
                TestCaseResults = results;
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Error);
            }
        }
    }

    [Display("List Test Cases", Groups: new string[] { "Yardstick", "V1 API" }, Description: "Get Yardstick Test Case List")]
    public class ListTestCasesStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "List of Yardstick test cases as string", Group: "Output")]
        [Output]
        public List<TestCase> TestCaseList { get; set; }

        #endregion

        public ListTestCasesStep()
        {
            Name = "List Test Cases";
        }

        public override void Run()
        {
            TestCaseList = Yardstick.V1GetTestCaseList();

            if (TestCaseList != null)
            {
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }

    public abstract class RunTestCaseStep : SuggestTestCasesStep
    {
        #region Settings

        //[Display(Name: "Test Case Name", Group: "Settings")]
        //[SuggestedValues("TCNames")]
        //public string TestCaseName { get; set; }

        //public List<string> TCNames { get; protected set; }

        [Display(Name: "Task Id", Group: "Output")]
        [Output]
        public string TaskId { get; set; }

        protected TestCaseStage Stage { get; set; }

        #endregion

        public RunTestCaseStep()
        {
            //Rules.Add(() => !string.IsNullOrEmpty(TestCaseName), "A Test Case Name is required.", "TestCaseName");
        }

        public override void Run()
        {
            //TaskId = Yardstick.RunTestCase(TestCaseName.Value);
            Tuple<string, int> test = Yardstick.RunTestCase(TestCaseName, Stage);

            if (test != null)
            {
                TaskId = test.Item1;
                int status = test.Item2;

                switch (status)
                {
                    case -1:
                        UpgradeVerdict(Verdict.Error);
                        break;
                    case 0:
                        UpgradeVerdict(Verdict.NotSet);
                        break;
                    case 1:
                        UpgradeVerdict(Verdict.Pass);
                        break;
                    case 2:
                        UpgradeVerdict(Verdict.Fail);
                        break;
                    default:
                        UpgradeVerdict(Verdict.Inconclusive);
                        break;
                }
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }


    [Display("Run Test Case", Groups: new string[] { "Yardstick", "V1 API" }, Description: "Run Yardstick Test Case")]
    public class RunReleasedTestCaseStep : RunTestCaseStep
    {
        public RunReleasedTestCaseStep()
        {
            Name = "Run Test Case";
            Stage = TestCaseStage.Release;
            //TCNames = Yardstick.AvailableTestCases;
        }
    }

    [Display("Run Sample Test Case", Groups: new string[] { "Yardstick", "V1 API" }, Description: "Run Yardstick Sample Test Case")]
    public class RunSampleTestCaseStep : RunTestCaseStep
    {
        public RunSampleTestCaseStep()
        {
            Name = "Run Sample Test Case";
            Stage = TestCaseStage.Sample;
            //TCNames = Yardstick.AvailableTestCases;
        }
    }

    [Display("Get Task Log", Groups: new string[] { "Yardstick", "V1 API" }, Description: "Get Task Log")]
    public class GetTaskLogStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Task Id", Group: "Settings")]
        public Input<string> TaskId { get; set; }

        [Display(Name: "Log Output", Group: "Output")]
        [Output]
        public List<string> Data { get; set; }

        #endregion

        public GetTaskLogStep()
        {
            Name = "Get Task Log";
            TaskId = new Input<string>();
            Rules.Add(() => TaskId.Step != null, "A Task Id is required.", "TaskId");
        }

        public override void Run()
        {
            Data = Yardstick.GetTaskLog(TaskId.Value);

            if (Data != null)
            {
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }

    [Display("Get Task Status", Groups: new string[] { "Yardstick", "V1 API" }, Description: "Get Task Status")]
    public class GetTaskStatusStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Task Id", Group: "Settings")]
        public Input<string> TaskId { get; set; }

        [Display(Name: "Task Status. 0->running, 1->pass, 2->fail.", Group: "Output")]
        [Output]
        public int Status { get; set; }

        #endregion

        public GetTaskStatusStep()
        {
            Name = "Get Task Status";
            TaskId = new Input<string>();
            Rules.Add(() => TaskId.Step != null, "A Task Id is required.", "TaskId");
        }

        public override void Run()
        {
            Status = Yardstick.GetTaskStatus(TaskId.Value);
            switch (Status)
            {
                case 0:
                    // pass indicating it was able to retrieve the status, not the actual value of the status
                    UpgradeVerdict(Verdict.Pass);
                    break;
                case 1:
                    // pass indicating it was able to retrieve the status, not the actual value of the status
                    UpgradeVerdict(Verdict.Pass);
                    break;
                case 2:
                    // pass indicating it was able to retrieve the status, not the actual value of the status
                    UpgradeVerdict(Verdict.Pass);
                    break;
                default:
                    // fail indicating it was not able to retrieve the status correctly
                    UpgradeVerdict(Verdict.Fail);
                    break;
            }
        }
    }


    [Display("Wait For Task", Groups: new string[] { "Yardstick", "V1 API" }, Description: "Wait For Task to Finish Execution")]
    public class WaitForTaskToFinish : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Task Id", Group: "Settings")]
        public Input<string> TaskId { get; set; }

        [Display(Name: "Task Status. 0->running, 1->pass, 2->fail.", Group: "Output")]
        [Output]
        public int Status { get; set; }

        #endregion

        public WaitForTaskToFinish()
        {
            Name = "Wait For Task";
            TaskId = new Input<string>();
            Rules.Add(() => TaskId.Step != null, "A Task Id is required.", "TaskId");
        }

        public override void Run()
        {
            while (Yardstick.GetTaskStatus(TaskId.Value) == 0)
            {
                TapThread.Sleep(1000);
            }
            UpgradeVerdict(Verdict.Pass);
        }
    }

    [Display("Run Test Suite", Groups: new string[] { "Yardstick", "V1 API" }, Description: "Run Yardstick Test Suite")]
    public class RunTestSuiteStep : SuggestTestSuitesStep
    {
        #region Settings

        //[Display(Name: "Test Suite Name", Group: "Settings")]
        //[SuggestedValues("TSNames")]
        //public string TestSuiteName { get; set; }

        [Display(Name: "Task Id", Group: "Output")]
        [Output]
        public string TaskId { get; set; }

        #endregion

        public RunTestSuiteStep()
        {
            Name = "Run Test Suite!";
            //TSNames = Yardstick.AvailableTestSuites;

            Rules.Add(() => !string.IsNullOrEmpty(TestSuiteName), "A Test Suite Name is required.", "TestSuiteName");
        }

        public override void Run()
        {
            Tuple<string, int> test = Yardstick.RunTestSuite(TestSuiteName);

            if (test != null)
            {
                TaskId = test.Item1;
                int status = test.Item2;

                switch (status)
                {
                    case -1:
                        UpgradeVerdict(Verdict.Error);
                        break;
                    case 0:
                        UpgradeVerdict(Verdict.NotSet);
                        break;
                    case 1:
                        UpgradeVerdict(Verdict.Pass);
                        break;
                    case 2:
                        UpgradeVerdict(Verdict.Fail);
                        break;
                    default:
                        UpgradeVerdict(Verdict.Inconclusive);
                        break;
                }
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }

    }




    [Display("Prepare Environment", Groups: new string[] { "Yardstick", "V1 API" }, Description: "Prepare environment")]
    public class PrepareEnvironment : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Action", Group: "Settings")]
        public PrepareEnvironmentAction Action { get; set; }

        [Display(Name: "Status", Group: "Ouput")]
        [Output]
        public int Status { get; set; }
        #endregion

        public PrepareEnvironment()
        {
            Name = "Prepare environment";
        }

        public override void Run()
        {
            Tuple<string, int> result = Yardstick.PrepareEnvironment(Action);

            if (result != null)
            {
                //TaskId = result.Item1;
                Status = result.Item2;

                switch (Status)
                {
                    case -1:
                        UpgradeVerdict(Verdict.Error);
                        break;
                    case 0:
                        UpgradeVerdict(Verdict.NotSet);
                        break;
                    case 1:
                        UpgradeVerdict(Verdict.Pass);
                        break;
                    case 2:
                        UpgradeVerdict(Verdict.Fail);
                        break;
                    default:
                        UpgradeVerdict(Verdict.Inconclusive);
                        break;
                }
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }

}