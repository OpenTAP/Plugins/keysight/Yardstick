﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using OpenTap.Plugins.Yardstick.Objects;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OpenTap.Plugins.Yardstick
{
    [Display("Delete Test Case", Groups: new string[] { "Yardstick", "V2 API", "TestCases" }, Description: "Delete Test Case")]
    public class DeleteTestCaseStep : SuggestTestCasesStep
    {
        #region Settings

        //[Display(Name: "Test Case Name", Group: "Settings")]
        //[SuggestedValues("TCNames")]
        //public string TestCaseName { get; set; }

        #endregion

        public DeleteTestCaseStep()
        {
            Name = "Delete Test Case";

            Rules.Add(() => !string.IsNullOrEmpty(TestCaseName), "A test case name is required.", "TestCaseName");
        }

        public override void Run()
        {
            bool result = Yardstick.DeleteTestCase(TestCaseName);

            if (result)
            {
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }

    [Display("Get Test Cases", Groups: new string[] { "Yardstick", "V2 API", "TestCases" }, Description: "Get list of test cases")]
    public class GetTestCasesStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Test case names", Group: "Output")]
        [Output]
        public List<TestCase> TestCases { get; set; }

        #endregion

        public GetTestCasesStep()
        {
            Name = "Get test cases";
        }

        public override void Run()
        {
            List<TestCase> testCases = Yardstick.V2GetTestCaseList();

            if (testCases != null)
            {
                TestCases = testCases;
                UpgradeVerdict(Verdict.Pass);
            }
            else
                UpgradeVerdict(Verdict.Fail);
        }
    }

    [Display("Upload Test Case", Groups: new string[] { "Yardstick", "V2 API", "TestCases" }, Description: "Upload Test Case File")]
    public class UploadTestCaseFile : YardstickTestStep
    {
        private string fileContentAsBase64 = null;
        #region Settings

        [Display(Name: "Test Case Name", Group: "Settings", Description: "The name of the test case to upload.")]
        public string TestCaseName { get; set; }

        [FilePath()]
        [Display(Name: "Test case file", Description: "Test case file url.", Group: "Settings")]
        public string FileBase64
        {
            get
            {
                return fileContentAsBase64;
            }
            set
            {
                bool fallback = true;
                FileInfo fi = null;
                try
                {
                    fi = new FileInfo(value);
                    if (fi.Exists)
                    {
                        string fileContent = File.ReadAllText(value);
                        fileContentAsBase64 = Base64Encoding.EncodeBase64(fileContent);
                        fallback = false;
                    }
                }
                catch (System.Exception ex)
                {
                    Log.Error(ex.Message);
                }
                

                if (fallback)
                {
                    if (Base64Encoding.IsBase64(value))
                    {
                        fileContentAsBase64 = value;
                    }
                    else
                    {
                        fileContentAsBase64 = null;
                    }
                }
            }
        }

        #endregion

        public UploadTestCaseFile()
        {
            Name = "Upload Test Case File";

            Rules.Add(() => !string.IsNullOrEmpty(TestCaseName), "A test case name is required.", "TestCaseName");
            Rules.Add(() => !string.IsNullOrEmpty(FileBase64), "A test case file is required.", "FileBase64");
        }

        public override void Run()
        {
            // Make sure that a file is loaded into the memory stream
            byte[] content = Encoding.UTF8.GetBytes(Base64Encoding.DecodeBase64(fileContentAsBase64));

            // Upload openrc file
            V2UploadedTestCaseResult result = Yardstick.UploadTestCaseFile(content, TestCaseName);

            if (result != null)
            {
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }

    [Display("Get Test Case", Groups: new string[] { "Yardstick", "V2 API", "TestCases" }, Description: "Get Test Case Info")]
    public class GetTestCaseStep : SuggestTestCasesStep
    {
        #region Settings

        //[Display(Name: "Test Case Name", Group: "Settings")]
        //[SuggestedValues("TCNames")]
        //public string TestCaseName { get; set; }

        [Display(Name: "Test Case Content.", Group: "Output")]
        [Output]
        public string TestCaseContent { get; set; }

        #endregion

        public GetTestCaseStep()
        {
            Name = "Get Test Case";

            Rules.Add(() => !string.IsNullOrEmpty(TestCaseName), "A test case name is required.", "TestCaseName");
        }

        public override void Run()
        {
            V2GetTestCaseResult result = Yardstick.GetTestCase(TestCaseName);

            if (result != null)
            {
                TestCaseContent = result.TestCaseContent;
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }

}
