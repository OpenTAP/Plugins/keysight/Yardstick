﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using OpenTap.Plugins.Yardstick.Objects;
using System;
using System.Collections.Generic;

namespace OpenTap.Plugins.Yardstick
{
    [Display("Create Test Suite", Groups: new string[] { "Yardstick", "V2 API", "Suite" }, Description: "Create Yardstick Test Suite")]
    public class CreateTestSuiteStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Test Suite Name", Group: "Settings", Description: "Name of test suite to create")]
        public String TestSuiteName { get; set; }

        [Display(Name: "Test Case Names", Group: "Settings", Description: "Test cases to add to test suite")]
        public List<string> TestCaseNames { get; set; }

        #endregion

        public CreateTestSuiteStep()
        {
            Name = "Create Test Suite";
            Rules.Add(() => !string.IsNullOrEmpty(TestSuiteName), "Test Suite Name is missing!", "TestSuiteName");
        }

        public override void Run()
        {
            string suiteName = Yardstick.CreateTestSuite(TestSuiteName, TestCaseNames);

            if (suiteName != null)
            {
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }

    [Display("Delete Test Suite", Groups: new string[] { "Yardstick", "V2 API", "Suite" }, Description: "Delete Test Suite")]
    public class DeleteTestSuiteStep : SuggestTestSuitesStep
    {
        public DeleteTestSuiteStep()
        {
            Name = "Delete Test Suite";

            Rules.Add(() => !string.IsNullOrEmpty(TestSuiteName), "A test suite name is required.", "TestSuiteName");
        }

        public override void Run()
        {
            bool deleted = Yardstick.DeleteTestSuite(TestSuiteName);
            if (deleted == true)
                UpgradeVerdict(Verdict.Pass);
            else
                UpgradeVerdict(Verdict.Fail);
        }
    }

    [Display("Get Test Suite Info", Groups: new string[] { "Yardstick", "V2 API", "Suite" }, Description: "Get Test Suite Info")]
    public class GetTestSuiteInfoStep : SuggestTestSuitesStep
    {
        #region Settings

        [Display(Name: "Test Suite Content", Group: "Output")]
        [Output]
        public string TestSuiteContent { get; set; }

        #endregion

        public GetTestSuiteInfoStep()
        {
            Name = "Get Test Suite Info";

            Rules.Add(() => !string.IsNullOrEmpty(TestSuiteName), "A test suite name is required.", "TestSuiteName");
        }

        public override void Run()
        {
            V2GetTestSuiteResult result = Yardstick.GetTestSuite(TestSuiteName);

            if (result != null)
            {
                TestSuiteContent = result.TestSuiteContent;
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
            //this.OnPropertyChanged("TestSuiteContent");
        }
    }

    [Display("Get Test Suite List", Groups: new string[] { "Yardstick", "V2 API", "Suite" }, Description: "Get Test Suite List")]
    public class GetTestSuiteListStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Test Suite Names", Group: "Output")]
        [Output]
        public List<string> TestSuiteNames { get; set; }

        #endregion

        public GetTestSuiteListStep()
        {
            Name = "Get Test Suite List";
        }

        public override void Run()
        {
            List<string> testSuiteList = Yardstick.GetTestSuiteList();

            if (testSuiteList != null)
            {
                TestSuiteNames = testSuiteList;
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }

}
