﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using OpenTap.Plugins.Yardstick.Objects;
using OpenTap.Plugins.Yardstick.RestClients;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OpenTap.Plugins.Yardstick.Instruments
{
    public enum TestCaseStage
    {
        Release,
        Sample
    }

    public class YardstickException : Exception
    {
        private static string FormatMessage(string message)
        {
            return message.Replace("\n", "");
        }

        public YardstickException(string message)
            : base(YardstickException.FormatMessage(message))
        {
        }
    }


    [Display(Name: "Yardstick Instrument", Groups: new string[] {"Yardstick"})]
    public class YardstickInstrument : Instrument
    {
        #region private fields

        private string baseUrl = null;
        //private List<string> availableTestCases = null;
        private readonly object availableTestCasesLock = new object();

        //private List<string> availableTestSuites = null;
        private readonly object availableTestSuitesLock = new object();

        #endregion

        #region Settings 

        [Display(Name: "Yardstick REST API Base Url", Group: "Settings")]
        public string BaseUrl
        {
            get
            {
                return baseUrl;
            }
            set
            {
                lock (availableTestCasesLock)
                {
                    //availableTestCases = null;
                    baseUrl = value;
                }
            }
        }

        #endregion

        //public List<string> AvailableTestCases
        //{
        //    get
        //    {
        //        TapThread.Start(() =>
        //        {
        //            lock (availableTestCasesLock)
        //            {
        //                if (availableTestCases == null)
        //                {
        //                    availableTestCases = new List<string>();
        //                    List<TestCase> testCases = V2GetTestCaseList(false);
        //                    if (testCases != null)
        //                    {
        //                        foreach (TestCase testCase in testCases)
        //                        {
        //                            availableTestCases.Add(testCase.Name);
        //                        }
        //                    }
        //                }
        //            }
        //        });
        //        return availableTestCases;
        //    }
        //}

        //public List<string> AvailableTestSuites
        //{
        //    get
        //    {
        //        TapThread.Start(() =>
        //        {
        //            lock (availableTestSuitesLock)
        //            {
        //                if (availableTestSuites == null)
        //                {
        //                    availableTestSuites = new List<string>();
        //                    List<string> testSuites = GetTestSuiteList(false);
        //                    if (testSuites != null)
        //                    {
        //                        foreach (string testSuite in testSuites)
        //                        {
        //                            availableTestSuites.Add(testSuite);
        //                        }
        //                    }
        //                }
        //            }
        //        });
        //        return availableTestSuites;
        //    }
        //}

        public YardstickInstrument()
        {
            // Yardstick
            BaseUrl = "http://10.114.182.33:8888";
            Name = "Yardstick";

            Rules.Add(() => !string.IsNullOrEmpty(BaseUrl), "A base url is required.", "BaseUrl");
        }

        /// <summary>
        /// Closes the connection to the Identity Service
        /// </summary>
        public override void Close()
        {
            base.Close();
            Log.Info("Yardstick REST close");
        }

        /// <summary>
        /// Opens the connection to the Identity Service
        /// </summary>
        public override void Open()
        {
            base.Open();
            Log.Info("Yardstick REST open");

            Task<RestClientResult<bool>> taskIsVersion1APISupported = YardstickRestClient.IsVersion1APISupported(BaseUrl);
            taskIsVersion1APISupported.Wait();
            if(taskIsVersion1APISupported.Result.Result == true)
            {
                Log.Info("Yardstick REST API v1 supported");
            }
            else
            {
                Log.Error("Yardstick REST API v1 not supported!");
                throw new System.Exception("Excution cannot continue");
            }


            Task<RestClientResult<bool>> taskIsVersion2APISupported = YardstickRestClient.IsVersion2APISupported(BaseUrl);
            taskIsVersion2APISupported.Wait();
            if (taskIsVersion2APISupported.Result.Result == true)
            {
                Log.Info("Yardstick REST API v2 supported");
            }
            else
            {
                Log.Error("Yardstick REST API v2 not supported!");
                throw new System.Exception("Excution cannot continue");
            }
        }

        public void PrintLogInfo<T>(IEnumerable<T> array)
        {
            foreach (T item in array)
            {
                //Log.Info(array.ToString());
                Log.Info(item.ToString());
            }
        }

        private int WaitForTaskToComplete(string taskId)
        {
            int status = -1;
            Log.Info($"The Task {taskId} was started!");

            var sw = System.Diagnostics.Stopwatch.StartNew();
            while (true)
            {
                Task<RestClientResult<int>> taskGetTaskStatus = YardstickRestClient.GetTaskStatus(BaseUrl, taskId);
                taskGetTaskStatus.Wait();
                status = taskGetTaskStatus.Result.Result;
                if (status == -1)
                {
                    Log.Error($"Get Task Status failed! Details: {taskGetTaskStatus.Result.Message}");
                    break;
                }
                else
                {
                    if (status == 0)
                    {
                        TapThread.Sleep(1000);
                    }
                    else if (status == 1)
                    {
                        Log.Info($"Task {taskId} -> Verdict: PASS!");
                        break;
                    }
                    else if (status == 2)
                    {
                        Log.Info($"Task {taskId} -> Verdict: FAIL!");
                        break;
                    }
                    else
                    {
                        Log.Info($"Task {taskId} -> Unknown status: {status}!");
                        break;
                    }
                }
            }
            return status;
        }

        private int WaitForV2TaskToComplete(string taskId)
        {
            int status = -1;
            Log.Info($"The Task {taskId} was started!");

            var sw = System.Diagnostics.Stopwatch.StartNew();
            while (true)
            {
                Task<RestClientResult<V2TaskInfoResult>> task = YardstickRestClient.GetTaskInformation(BaseUrl, taskId);
                task.Wait();
                V2TaskInfoResult result = task.Result.Result;

                if (result == null)
                {
                    Log.Error($"Get Task Status failed! Details: {task.Result.Message}");
                }

                status = result.Task.Status;
                if (status == -1)
                {
                    Log.Error($"Get Task Status failed! Details: {task.Result.Message}");
                    break;
                }
                else
                {
                    if (status == 0)
                    {
                        TapThread.Sleep(1000);
                    }
                    else if (status == 1)
                    {
                        // todo: The result might actually indicate the the test failed!
                        Log.Info($"Task {taskId} -> Verdict: PASS!");
                        break;
                    }
                    else if (status == 2)
                    {
                        Log.Info($"Task {taskId} -> Verdict: FAIL!");
                        break;
                    }
                    else
                    {
                        Log.Info($"Task {taskId} -> Unknown status: {status}!");
                        break;
                    }
                }
            }
            return status;
        }

        #region V1 API

        /// <summary>
        /// Get Results for given Task.  This API is used to get the test results of certain task. If you call 
        /// /yardstick/testcases/samples/action API, it will return a task id. You can use the returned task id to get
        /// the results by using this API.
        /// </summary>
        /// <param name="taskId">The task Id for which results should be retrieved.</param>
        /// <returns>The list of results for the given task.</returns>
        public string GetResult(string taskId)
        {
            string result = null;
            Log.Debug("-- Get Result ----------------------------------------------------------------------------------");
            Task<RestClientResult<string>> taskGetResult = YardstickRestClient.GetResult(BaseUrl, taskId);
            taskGetResult.Wait();
            result = taskGetResult.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Get Result execution failed. Details: {taskGetResult.Result.Message}");
            }
            else
            {
                Log.Info("Get Result returned following content:");
                Log.Info(result);
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return result;
        }

        /// <summary>
        /// Run OPNFV TestSuite.
        /// 
        /// This API is used to run a yardstick test suite.
        /// </summary>
        /// <param name="testSuiteName">The name of the test suite to be run.</param>
        /// <returns>A Guid which represents the task id which executes the test case.</returns>
        public Tuple<string, int> RunTestSuite(string testSuiteName)
        {
            string taskId = null;
            int status = -1;
            Log.Debug("-- Get Test Suite List --------------------------------------------------------------------------");
            Task<RestClientResult<string>> taskRunTestSuite = YardstickRestClient.RunTestSuite(BaseUrl, testSuiteName);
            taskRunTestSuite.Wait();
            taskId = taskRunTestSuite.Result.Result;
            if (taskId == null)
            {
                throw new YardstickException($"Run Test Suite execution failed. Details: {taskRunTestSuite.Result.Message}");
                throw new System.Exception("Excution cannot continue");
            }
            else
            {
                Log.Info($"Test Case {testSuiteName} was scheduled successfully!");
                Log.Info($"The Task {taskId} was started!");

                var sw = System.Diagnostics.Stopwatch.StartNew();
                while (true)
                {
                    Task<RestClientResult<int>> taskGetTaskStatus = YardstickRestClient.GetTaskStatus(BaseUrl, taskId);
                    taskGetTaskStatus.Wait();
                    status = taskGetTaskStatus.Result.Result;
                    if (status == -1)
                    {
                        throw new YardstickException($"Get Task Status failed! Details: {taskGetTaskStatus.Result.Message}");
                        //break;
                    }
                    else
                    {
                        if (status == 0)
                        {
                            TapThread.Sleep(1000);
                        }
                        else if (status == 1)
                        {
                            Log.Info($"Task {taskId} -> Verdict: PASS!");
                            break;
                        }
                        else if (status == 2)
                        {
                            Log.Info($"Task {taskId} -> Verdict: FAIL!");
                            break;
                        }
                        else
                        {
                            Log.Info($"Task {taskId} -> Unknown status: {status}!");
                            break;
                        }
                    }
                }
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return new Tuple<string, int>(taskId, status);
        }

        /// <summary>
        /// Get a list of release test cases.
        /// 
        /// This API is used to list all release test cases now in yardstick.
        /// </summary>
        /// <param name="log">Print detailed log information or not</param>
        /// <returns>The list of results for the given task.</returns>
        public List<TestCase> V1GetTestCaseList(bool log = true)
        {
            Log.Debug("-- Get Test Cases ------------------------------------------------------------------------------");
            Task<RestClientResult<List<TestCase>>> task = YardstickRestClient.V1GetTestCaseList(BaseUrl);
            task.Wait();
            List<TestCase> testCases = task.Result.Result;
            if (testCases == null)
            {
                throw new YardstickException($"Get Test Case List operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Received {testCases.Count} test cases.");
                if (log)
                {
                    Log.Info("Get Test Case List returned following content:");
                    PrintLogInfo<TestCase>(testCases);
                }
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return testCases;
        }

        #endregion


        #region Environment Related

        /// <summary>
        /// Create a new environment
        /// </summary>
        /// <param name="environmentName">Name of environment to create</param>
        /// <returns>Id of the created environment</returns>
        public string CreateEnvironment(string environmentName)
        {
            string environmentId = null;
            Log.Debug("-- Create Environment --------------------------------------------------------------------------");
            Task<RestClientResult<string>> taskCreateEnvironments = YardstickRestClient.CreateEnvironment(BaseUrl, environmentName);
            taskCreateEnvironments.Wait();
            environmentId = taskCreateEnvironments.Result.Result;
            if (environmentId == null)
            {
                throw new YardstickException($"Create Environment operation failed! Details: {taskCreateEnvironments.Result.Message}");
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return environmentId;
        }

        /// <summary>
        /// Delete yardstick environment
        /// </summary>
        /// <param name="environmentId">Id of the environment to delete</param>
        /// <returns>If the operation was a success</returns>
        public bool DeleteEnvironment(string environmentId)
        {
            bool succeeded = false;
            Log.Debug("-- Delete Environment ------------------------------------------------------------------------------");
            Task<RestClientResult<V2DeletedEnvironmentResult>> task = YardstickRestClient.DeleteEnvironment(BaseUrl, environmentId);
            task.Wait();
            V2DeletedEnvironmentResult result = task.Result.Result;
            if (result == null)
            {
                //Log.Error($"Delete Environment operation failed! Details: {task.Result.Message}");
                throw new YardstickException($"Delete Environment operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Task {result.EnvironmentId} was deleted.");
                succeeded = true;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return succeeded;


            //bool deleted = false;
            //Log.Debug("-- Delete Environment --------------------------------------------------------------------------");

            //if (!string.IsNullOrEmpty(environmentId))
            //{
            //    Task<RestClientResult<bool>> taskDeleteEnvironment = YardstickRestClient.DeleteEnvironment(BaseUrl, environmentId);
            //    taskDeleteEnvironment.Wait();
            //    deleted = taskDeleteEnvironment.Result.Result;
            //    if (deleted == false)
            //    {
            //        Log.Error($"Delete Environment operation failed! Details: {taskDeleteEnvironment.Result.Message}");
            //    }
            //}
            //else
            //{
            //    Log.Error($"EnvironmentId should not be null or empty!");
            //}
            //Log.Debug("------------------------------------------------------------------------------------------------");
            //return deleted;
        }

        /// <summary>
        /// Get list of yardstick environments
        /// </summary>
        /// <returns>List of yardstick environments</returns>
        public List<Objects.Environment> ListEnvironments()
        {
            List<Objects.Environment> environments = null;
            Log.Debug("-- List Environments ---------------------------------------------------------------------------");
            Task<RestClientResult<List<Objects.Environment>>> taskListEnvironments = YardstickRestClient.ListEnvironments(BaseUrl);
            taskListEnvironments.Wait();
            environments = taskListEnvironments.Result.Result;
            if (environments == null)
            {
                throw new YardstickException($"List Environments operation failed! Details: {taskListEnvironments.Result.Message}");
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return environments;
        }

        /// <summary>
        /// Prepare yardstick environment
        /// </summary>
        /// <param name="action">The preparation action to perform</param>
        /// <returns></returns>
        public Tuple<string, int> PrepareEnvironment(PrepareEnvironmentAction action)
        {
            string taskId = null;
            int status = -1;
            Log.Debug("-- Get Projects --------------------------------------------------------------------------------");
            Task<RestClientResult<string>> taskPrepareEnvironment = YardstickRestClient.PrepareEnvironment(BaseUrl, action);
            taskPrepareEnvironment.Wait();
            taskId = taskPrepareEnvironment.Result.Result;
            if (taskId == null)
            {
                throw new YardstickException($"Preparing environment failed. Details: {taskPrepareEnvironment.Result.Message}");
            }
            else
            {
                Log.Info($"Preparing environment was scheduled successfully!");
                status = WaitForTaskToComplete(taskId);
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return new Tuple<string, int>(taskId, status);
        }


        /// <summary>
        /// Upload certificate
        /// </summary>
        /// <param name="openrcFileContent">Content of certificate file</param>
        /// <param name="environmentId">Environment to apply certificate to</param>
        /// <returns></returns>
        public V2OpenrcUploadedResult UploadOpenrcFile(byte[] openrcFileContent, string environmentId)
        {
            Log.Debug("-- Get Projects --------------------------------------------------------------------------------");
            Task<RestClientResult<V2OpenrcUploadedResult>> task = YardstickRestClient.UploadOpenrcFile(BaseUrl, openrcFileContent, environmentId);
            task.Wait();

            V2OpenrcUploadedResult result = task.Result.Result;
            if (result == null)
            {
                //Log.Error($"Uploading openrc operation failed! Details: {task.Result.Message}");
                throw new YardstickException($"Uploading openrc operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Uploaded openrc file.");
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return result;
        }

        #endregion


        #region Test Case Related

        /// <summary>
        /// Deletes a Yardstick test case with the given name.
        /// </summary>
        /// <returns>Returns a boolean indicating whether the operation succeeded or not.</returns>
        public bool DeleteTestCase(string testCaseName)
        {
            bool succeeded = false;
            Log.Debug("-- Delete Test Case ------------------------------------------------------------------------------");
            Task<RestClientResult<V2DeletedTestCaseResult>> task = YardstickRestClient.DeleteTestCase(BaseUrl, testCaseName);
            task.Wait();
            V2DeletedTestCaseResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Delete Test Case operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Task {result.testCaseName} was deleted.");
                succeeded = true;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return succeeded;
        }

        /// <summary>
        /// Get information about a test case
        /// </summary>
        /// <param name="testCaseName">The name of the test case to lookup</param>
        /// <returns>Test case information</returns>
        public V2GetTestCaseResult GetTestCase(string testCaseName)
        {
            Log.Debug("-- Get Test Case ------------------------------------------------------------------------------");
            Task<RestClientResult<V2GetTestCaseResult>> task = YardstickRestClient.GetTestCase(BaseUrl, testCaseName);
            task.Wait();
            V2GetTestCaseResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Get Test Case operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Received testcase by name {testCaseName}.");
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return result;
        }

        /// <summary>
        /// Run OPNFV TestCase.
        /// 
        /// This API is used to run a yardstick release test case.
        /// </summary>
        /// <param name="testCaseName">The name of the test case to be run.</param>
        /// <returns>A Guid which represents the task id which executes the test case.</returns>
        public Tuple<string, int> RunTestCase(string testCaseName, TestCaseStage stage)
        {
            string taskId = null;
            Log.Debug("-- Run Test Case ------------------------------------------------------------------------------");
            int status = -1;
            string strStage = stage == TestCaseStage.Sample ? "sample" : "release";

            Task<RestClientResult<V1V2RunTestCaseResult>> task = YardstickRestClient.RunTestCase(BaseUrl, testCaseName, strStage);
            task.Wait();
            V1V2RunTestCaseResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Run Test Case execution failed. Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Test Case {testCaseName} was scheduled successfully!");
                taskId = result.TaskId;
                status = WaitForTaskToComplete(result.TaskId);
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return new Tuple<string, int>(taskId, status);
        }


        /// <summary>
        /// Create a new test case based on the file uploaded
        /// </summary>
        /// <param name="testCaseFileContent">Content of the test case file to create</param>
        /// <param name="testCaseName">Test case name to create</param>
        /// <returns></returns>
        public V2UploadedTestCaseResult UploadTestCaseFile(byte[] testCaseFileContent, string testCaseName)
        {
            Log.Debug("-- Get Projects --------------------------------------------------------------------------------");
            Task<RestClientResult<V2UploadedTestCaseResult>> task = YardstickRestClient.UploadTestCaseFile(BaseUrl, testCaseFileContent, testCaseName);
            task.Wait();

            V2UploadedTestCaseResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Uploading test case operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Uploaded test case file.");
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return result;
        }

        /// <summary>
        /// Get a list of release test cases.
        /// 
        /// This API is used to list all release test cases now in yardstick.
        /// </summary>
        /// <param name="log">Print detailed log information or not</param>
        /// <returns>The list of results for the given task.</returns>
        public List<TestCase> V2GetTestCaseList(bool log = true)
        {
            List<TestCase> ret = null;
            Log.Debug("-- Get Test Cases ------------------------------------------------------------------------------");
            Task<RestClientResult<V2GetTestCasesResult>> task = YardstickRestClient.V2GetTestCaseList(BaseUrl);
            task.Wait();
            V2GetTestCasesResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Get Test Case List operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Received {result.TestCases.Count} test cases.");
                if (log)
                {
                    Log.Info("Get Test Case List returned following content:");
                    PrintLogInfo<TestCase>(result.TestCases);
                }
                ret = result.TestCases;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return ret;
        }

        #endregion

        #region Task Related

        /// <summary>
        /// Add an environment to a task.
        /// </summary>
        /// <param name="taskId">The task id which identifies the Yardstick task to which the environment should be added.</param>
        /// <param name="environmentId">The environment id.</param>
        /// <returns></returns>
        public bool AddEnvironmentToTask(string taskId, string environmentId)
        {
            bool succeeded = false;
            Log.Debug("-- Add Environment To A Task -------------------------------------------------------------------");
            Task<RestClientResult<bool>> taskAddEnvironment = YardstickRestClient.AddEnvironmentToTask(BaseUrl, taskId, environmentId);
            taskAddEnvironment.Wait();
            succeeded = taskAddEnvironment.Result.Result;
            if (succeeded == false)
            {
                throw new YardstickException($"Add Environment operation failed! Details: {taskAddEnvironment.Result.Message}");
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return succeeded;
        }

        public bool AddTestCaseToTask(string taskId, string testCaseName, string testCaseContent)
        {
            bool succeeded = false;
            Log.Debug("-- Add Test Case To Task --------------------------------------------------------------------------------");
            Task<RestClientResult<V2AddedCaseToTaskResult>> task = YardstickRestClient.AddTestCaseToTask(BaseUrl, taskId, testCaseName, testCaseContent);
            task.Wait();

            V2AddedCaseToTaskResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Add Test Case operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Test Case added successfully.");
                succeeded = true;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return succeeded;

            //bool succeeded = false;
            //Log.Debug("-- Add Test Case To A Task -------------------------------------------------------------------");
            //Task<RestClientResult<bool>> taskAddTestCase = YardstickRestClient.AddTestCaseToTask(BaseUrl, taskId, testCaseName, testCaseContent);
            //taskAddTestCase.Wait();
            //succeeded = taskAddTestCase.Result.Result;
            //if (succeeded == false)
            //{
            //    Log.Error($"Add Test Case operation failed! Details: {taskAddTestCase.Result.Message}");
            //}
            //Log.Debug("------------------------------------------------------------------------------------------------");
            //return succeeded;
        }

        public bool AddTestSuiteToTask(string taskId, string testSuiteName, string testSuiteContent)
        {
            bool succeeded = false;
            Log.Debug("-- Add Test Suite To Task --------------------------------------------------------------------------------");
            Task<RestClientResult<V2AddedSuiteToTaskResult>> task = YardstickRestClient.AddTestSuiteToTask(BaseUrl, taskId, testSuiteName, testSuiteContent);
            task.Wait();

            V2AddedSuiteToTaskResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Add Test Suite operation failed! Details: {task.Result.Message}");
                throw new System.Exception("Excution cannot continue");
            }
            else
            {
                Log.Info($"Test Suite added successfully.");
                succeeded = true;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return succeeded;

            //bool succeeded = false;
            //Log.Debug("-- Add Test Suite To A Task -------------------------------------------------------------------");
            //Task<RestClientResult<bool>> taskAddTestSuite = YardstickRestClient.AddTestSuiteToTask(BaseUrl, taskId, testSuiteName, testSuiteContent);
            //taskAddTestSuite.Wait();
            //succeeded = taskAddTestSuite.Result.Result;
            //if (succeeded == false)
            //{
            //    Log.Error($"Add Test Suite operation failed! Details: {taskAddTestSuite.Result.Message}");
            //}
            //Log.Debug("------------------------------------------------------------------------------------------------");
            //return succeeded;
        }

        /// <summary>
        /// Creates a new Yardstick Task.
        /// </summary>
        /// <param name="taskName">The name of the task to be created</param>
        /// <param name="projectId">The project id in which the task shoudl be created.</param>
        /// <returns>Upon successful creation it will return the task id, otherwise null.</returns>
        public string CreateTask(string taskName, string projectId)
        {
            string ret = null;
            Log.Debug("-- Create Task --------------------------------------------------------------------------------");
            Task<RestClientResult<V2CreatedTaskResult>> task = YardstickRestClient.CreateTask(BaseUrl, taskName, projectId);
            task.Wait();

            V2CreatedTaskResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Create Task operation failed! Details: {task.Result.Message}");
                throw new System.Exception("Excution cannot continue");
            }
            else
            {
                Log.Info($"Task {taskName} was created.");
                ret = result.TaskId;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return ret;
        }

        /// <summary>
        /// Deletes a Yardstick task with the given task Id.
        /// </summary>
        /// <param name="taskId">Id of task to delete</param>
        /// <returns>Returns a boolean indicating whether the operation succeeded or not.</returns>
        public bool DeleteTask(string taskId)
        {
            bool succeeded = false;
            Log.Debug("-- Delete Task ------------------------------------------------------------------------------");
            Task<RestClientResult<V2DeletedTaskResult>> task = YardstickRestClient.DeleteTask(BaseUrl, taskId);
            task.Wait();
            V2DeletedTaskResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Delete Project operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Task {result.TaskId} was deleted.");
                succeeded = true;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return succeeded;
        }

        /// <summary>
        /// Gets a Project Info of an Yardstick project with the given Project Id.
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns>On successful creation it will return the uuid of the newly created project.</returns>
        public V2TaskInfoResult GetTaskInfo(string taskId)
        {
            V2TaskInfoResult taskInfoResult = null;
            Log.Debug("-- Get Task Info ----------------------------------------------------------------------------");
            Task<RestClientResult<V2TaskInfoResult>> taskGetTaskInfo = YardstickRestClient.GetTaskInformation(BaseUrl, taskId);
            taskGetTaskInfo.Wait();
            taskInfoResult = taskGetTaskInfo.Result.Result;
            if (taskInfoResult == null)
            {
                throw new YardstickException($"Get Task Info operation failed! Details: {taskGetTaskInfo.Result.Message}");
                throw new System.Exception("Excution cannot continue");
            }
            else
            {
                Log.Info("Get Project Info returned following content:");
                Log.Info(taskInfoResult.ToString());
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return taskInfoResult;
        }

        /// <summary>
        /// Get the log outputted by a task.
        /// </summary>
        /// <param name="taskId">The task id which identifies the Yardstick task.</param>
        /// <returns>A list of string representing the log.</returns>
        internal List<string> GetTaskLog(string taskId)
        {
            Log.Debug("-- Get Task Log --------------------------------------------------------------------------------");
            Task<RestClientResult<List<string>>> taskGetTaskLog = YardstickRestClient.GetTaskLog(BaseUrl, taskId);
            taskGetTaskLog.Wait();
            List<string> data = taskGetTaskLog.Result.Result;
            if (data == null)
            {
                throw new YardstickException($"Get Task Log failed! Details: {taskGetTaskLog.Result.Message}");
            }
            else
            {
                foreach (string logLine in data)
                {
                    Log.Info(logLine);
                }
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return data;
        }

        /// <summary>
        /// Get the status of the task.
        /// </summary>
        /// <param name="taskId">The task id which identifies the Yardstick task.</param>
        /// <returns>An integeter representing the status. 0->running, 1->pass, 2->fail.</returns>
        internal int GetTaskStatus(string taskId)
        {
            int status = -1;
            Log.Debug("-- Get Task Status -----------------------------------------------------------------------------");
            Task<RestClientResult<int>> taskGetTaskStatus = YardstickRestClient.GetTaskStatus(BaseUrl, taskId);
            taskGetTaskStatus.Wait();
            status = taskGetTaskStatus.Result.Result;
            if (status == -1)
            {
                throw new YardstickException($"Get Task Status failed! Details: {taskGetTaskStatus.Result.Message}");
            }
            else
            {
                string statusString = "";
                switch (status)
                {
                    case 0:
                        statusString = "running";
                        break;
                    case 1:
                        statusString = "pass";
                        break;
                    case 2:
                        statusString = "fail";
                        break;
                    default:
                        statusString = "?";
                        break;
                }
                Log.Info($"Task {taskId} -> status is {statusString}");
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return status;
        }

        public List<V2Taskinfo> V2GetTaskList(bool log = true)
        {
            List<V2Taskinfo> ret = null;
            Log.Debug("-- Get Tasks ------------------------------------------------------------------------------");
            Task<RestClientResult<V2GetTasksResult>> task = YardstickRestClient.GetTasks(BaseUrl);
            task.Wait();
            V2GetTasksResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Get Task List operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Received {result.Tasks.Count} tasks.");
                if (log)
                {
                    Log.Info("Get Task List returned following content:");
                    PrintLogInfo<V2Taskinfo>(result.Tasks);
                }
                ret = result.Tasks;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return ret;
        }

        public Tuple<string, int> RunTask(string taskId)
        {
            Log.Debug("-- Run Test Task ------------------------------------------------------------------------------");
            int status = -1;

            Task<RestClientResult<V2RunTaskResult>> task = YardstickRestClient.RunTask(BaseUrl, taskId);
            task.Wait();
            V2RunTaskResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Run Task execution failed. Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Task {taskId} was scheduled successfully!");
                taskId = result.TaskId;
                status = WaitForV2TaskToComplete(result.TaskId);
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return new Tuple<string, int>(taskId, status);
        }

        #endregion

        #region Suite Related

        public string CreateTestSuite(string testSuiteName, List<string> testCaseNames)
        {
            string ret = null;
            Log.Debug("-- Create Test Suite --------------------------------------------------------------------------------");
            Task<RestClientResult<V2CreatedTestSuiteResult>> task = YardstickRestClient.CreateTestSuite(BaseUrl, testSuiteName, testCaseNames);
            task.Wait();

            V2CreatedTestSuiteResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Create Test Suite operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Test suite {result.TestSuiteName} was created.");
                ret = result.TestSuiteName;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return ret;
        }

        /// <summary>
        /// Deletes a Yardstick test suite with the given name.
        /// </summary>
        /// <returns>Returns a boolean indicating whether the operation succeeded or not.</returns>
        public bool DeleteTestSuite(string testSuiteName)
        {
            bool succeeded = false;
            Log.Debug("-- Delete Test Suite ------------------------------------------------------------------------------");
            Task<RestClientResult<V2DeletedTestSuiteResult>> task = YardstickRestClient.DeleteTestSuite(BaseUrl, testSuiteName);
            task.Wait();
            V2DeletedTestSuiteResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Delete Test Suite operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Task {result.TestSuiteName} was deleted.");
                succeeded = true;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return succeeded;
        }

        /// <summary>
        /// Gets Info of a Yardstick Test Suite with the given test suite name.
        /// </summary>
        /// <returns>Test suite content.</returns>
        //public string GetTestSuite(string testSuiteName)
        //{
        //    string ret = null;
        //    Log.Debug("-- Get Test Suite ------------------------------------------------------------------------------");
        //    Task<RestClientResult<V2GetTestSuiteResult>> task = YardstickRestClient.GetTestSuite(BaseUrl, testSuiteName);
        //    task.Wait();
        //    V2GetTestSuiteResult result = task.Result.Result;
        //    if (result == null)
        //    {
        //        Log.Error($"Get Test Suite information operation failed! Details: {task.Result.Message}");
        //    }
        //    else
        //    {
        //        Log.Info($"Received Test Suite by name {testSuiteName}.");
        //        ret = result.TestSuiteContent;
        //    }
        //    Log.Debug("------------------------------------------------------------------------------------------------");
        //    return ret;
        //}

        public V2GetTestSuiteResult GetTestSuite(string testSuiteName)
        {
            Log.Debug("-- Get Test Suite ------------------------------------------------------------------------------");
            Task<RestClientResult<V2GetTestSuiteResult>> task = YardstickRestClient.GetTestSuite(BaseUrl, testSuiteName);
            task.Wait();
            V2GetTestSuiteResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Get Test Suite operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Received test suite by name {testSuiteName}.");
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return result;
        }

        /// <summary>
        /// Get a list of test suites.
        /// 
        /// This API is used to list all test suites now in yardstick.
        /// </summary>
        /// <returns>The list of results for the given task.</returns>
        public List<string> GetTestSuiteList(bool log = true)
        {
            List<string> ret = null;
            Log.Debug("-- Get Test Suites ------------------------------------------------------------------------------");
            Task<RestClientResult<V2GetTestSuitesResult>> task = YardstickRestClient.GetTestSuiteList(BaseUrl);
            task.Wait();
            V2GetTestSuitesResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Get Test Suites operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Received {result.TestSuites.Count} test suites.");
                ret = result.TestSuites;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return ret;
        }

        #endregion

        #region Project related

        /// <summary>
        /// Create an Yardstick project with the given name.
        /// </summary>
        /// <returns>On successful creation it will return the uuid of the newly created project.</returns>
        public string CreateProject(string projectName)
        {
            string ret = null;
            Log.Debug("-- Get Projects --------------------------------------------------------------------------------");
            Task<RestClientResult<V2CreatedProjectResult>> task = YardstickRestClient.CreateProject(BaseUrl, projectName);
            task.Wait();

            V2CreatedProjectResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Create Project operation failed! Details: {task.Result.Message}");
                throw new System.Exception("Excution cannot continue");
            }
            else
            {
                Log.Info($"Project {projectName} was created.");
                ret = result.ProjectId;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return ret;
        }

        /// <summary>
        /// Deletes an Yardstick project with the given Project Id.
        /// </summary>
        /// <returns>Returns a boolean indicating whether the operation succeeded or not.</returns>
        public bool DeleteProject(string projectId)
        {
            bool succeeded = false;
            Log.Debug("-- Delete Project ------------------------------------------------------------------------------");
            Task<RestClientResult<V2DeletedProjectResult>> task = YardstickRestClient.DeleteProject(BaseUrl, projectId);
            task.Wait();
            V2DeletedProjectResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Delete Project operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Task {result.ProjectId} was deleted.");
                succeeded = true;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return succeeded;
        }

        /// <summary>
        /// Get the list of tasks that are available in Yardstick.
        /// </summary>
        /// <returns></returns>
        public List<Project> GetProjects()
        {
            List<Project> ret = null;
            Log.Debug("-- Get Projects ------------------------------------------------------------------------------");
            Task<RestClientResult<V2GetProjectsResult>> task = YardstickRestClient.GetProjects(BaseUrl);
            task.Wait();
            V2GetProjectsResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Get Projects operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Received {result.Projects.Count} projects.");
                ret = result.Projects;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return ret;
        }

        /// <summary>
        /// Gets a Project Info of an Yardstick project with the given Project Id.
        /// </summary>
        /// <returns>On successful creation it will return the uuid of the newly created project.</returns>
        public Project GetProjectInfo(string projectId)
        {
            Project ret = null;
            Log.Debug("-- Get Test Cases ------------------------------------------------------------------------------");
            Task<RestClientResult<V2GetProjectResult>> task = YardstickRestClient.GetProjectInfo(BaseUrl, projectId);
            task.Wait();
            V2GetProjectResult result = task.Result.Result;
            if (result == null)
            {
                throw new YardstickException($"Get Project information operation failed! Details: {task.Result.Message}");
            }
            else
            {
                Log.Info($"Received project by name {result.Project.Name} and uuid {result.Project.UUID}.");
                ret = result.Project;
            }
            Log.Debug("------------------------------------------------------------------------------------------------");
            return ret;
        }

        #endregion

    }
}
