﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System;

namespace OpenTap.Plugins.Yardstick
{
    public abstract class CreateTaskStep : YardstickTestStep
    {
        #region Settings
        [Display(Name: "Task name", Group: "Settings")]
        public string TaskName { get; set; }

        [Display(Name: "Project id", Group: "Settings")]
        public Input<string> ProjectId { get; set; }

        [Display(Name: "Environment id", Group: "Settings")]
        public Input<string> EnvironmentId { get; set; }

        [Output]
        [Display(Name: "Task id", Group: "Output")]
        public string TaskId { get; set; }
        #endregion

        public CreateTaskStep()
        {
            ProjectId = new Input<string>();
            EnvironmentId = new Input<string>();

            Rules.Add(() => !string.IsNullOrEmpty(TaskName), "Task Name is missing!", "TaskName");
            Rules.Add(() => ProjectId.Step != null, "Project Id is required.", "ProjectId");
            Rules.Add(() => EnvironmentId.Step != null, "Project Id is required.", "EnvironmentId");
        }

        /// <summary>
        /// This function perform multiple tasks, 1) Create task, 2) Add test case to task, and 3) delete task if test case could not be added. Finally, it updates the test step output and verdict
        /// </summary>
        /// <param name="testCaseName">Test case name</param>
        /// <param name="testCaseContent">Test case content</param>
        protected void CreateTaskWithTestCase(string testCaseName, string testCaseContent)
        {
            // Create task
            string taskId = PrintExceptions<string>(() => Yardstick.CreateTask(TaskName, ProjectId.Value));

            try
            {
                // Add test case to task
                bool addedTestCaseToTask = PrintExceptions(() => Yardstick.AddTestCaseToTask(taskId, testCaseName, testCaseContent));

                // Add environment to task
                bool addedEnvToTask = PrintExceptions(() => Yardstick.AddEnvironmentToTask(taskId, EnvironmentId.Value));
            }
            catch(Exception)
            {
                // If test case could not be added, then delete task
                PrintExceptions(() => Yardstick.DeleteTask(taskId));

                // Rethrow exception
                throw;
            }

            TaskId = taskId;
            UpgradeVerdict(Verdict.Pass);
        }

        /// <summary>
        /// This function perform multiple tasks, 1) Create task, 2) Add test suite to task, and 3) delete task if test suite could not be added. Finally, it updates the test step output and verdict
        /// </summary>
        /// <param name="testCaseName">Test case name</param>
        /// <param name="testCaseContent">Test case content</param>
        protected void CreateTaskWithTestSuite(string testSuiteName, string testSuiteContent)
        {
            // Create task
            string taskId = PrintExceptions<string>(() => Yardstick.CreateTask(TaskName, ProjectId.Value));

            try
            {
                // Add test case to task
                bool addedTestSuiteToTask = PrintExceptions(() => Yardstick.AddTestSuiteToTask(taskId, testSuiteName, testSuiteContent));

                // Add environment to task
                bool addedEnvToTask = PrintExceptions(() => Yardstick.AddEnvironmentToTask(taskId, EnvironmentId.Value));
            }
            catch (Exception)
            {
                // If test case could not be added, then delete task
                PrintExceptions(() => Yardstick.DeleteTask(taskId));

                // Rethrow exception
                throw;
            }

            TaskId = taskId;
            UpgradeVerdict(Verdict.Pass);
        }
    }
}
