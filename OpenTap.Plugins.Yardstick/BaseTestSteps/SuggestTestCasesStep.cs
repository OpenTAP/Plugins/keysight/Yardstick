﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenTap.Plugins.Yardstick
{
    public interface ISuggestTestCaseStep
    {
        IEnumerable<string> TCNames { get; }

        string TestCaseName { get; set; }
    }

    public abstract class SuggestTestCasesStep : YardstickTestStep, ISuggestTestCaseStep
    {
        public IEnumerable<string> TCNames { get; protected set; }

        [Display(Name: "Test Case Name", Group: "Settings")]
        [SuggestedValues("TCNames")]
        public string TestCaseName { get; set; }

        protected void UpdateTestCases()
        {
            Task.Factory.StartNew(() => IgnoreExceptions<List<Objects.TestCase>>(() => Yardstick.V2GetTestCaseList(false)))
            .ContinueWith(result =>
            {
                if (result.Result == null)
                    return;

                TCNames = result.Result.Select((testCase) => testCase.Name);
                this.OnPropertyChanged("TCNames");
            }, TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        public SuggestTestCasesStep()
        {
            Rules.Add(() => !string.IsNullOrEmpty(TestCaseName), "A Test Case Name is required.", "TestCaseName");

            UpdateTestCases();
        }
    }

}
