﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System.IO;

namespace OpenTap.Plugins.Yardstick
{
    public abstract class UploadTestSuiteFileStep : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Test Suite Name", Group: "Settings", Description: "The name of the test suite to upload.")]
        public string TestSuiteName { get; set; }

        protected byte[] testSuiteContent;
        private string testSuiteFileUrl = "";

        [FilePath()]
        [Display(Name: "Test suite file", Description: "Test suite file url.", Group: "Settings")]
        public string TestSuiteFileUrl
        {
            get
            {
                return testSuiteFileUrl;
            }
            set
            {
                try
                {
                    byte[] tmp = File.ReadAllBytes(value);
                    testSuiteContent = tmp;

                    // Udate property value
                    testSuiteFileUrl = value;
                }
                catch
                {
                    testSuiteFileUrl = "";
                }
            }
        }

        #endregion

        public UploadTestSuiteFileStep()
        {
            Rules.Add(() => !string.IsNullOrEmpty(TestSuiteName), "A test suite name is required.", "TestSuiteName");
            Rules.Add(() => !string.IsNullOrEmpty(TestSuiteFileUrl), "A test suite file is required.", "TestSuiteFileUrl");
        }
    }

}
