﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Newtonsoft.Json;
using OpenTap.Plugins.Yardstick.Instruments;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace OpenTap.Plugins.Yardstick
{
    public abstract class YardstickTestStep : TestStep
    {
        #region Settings

        [Display(Name: "Yardstick Instrument", Group: "Instruments")]
        public YardstickInstrument Yardstick { get; set; }

        #endregion

        #region Helper functions

        protected TResult IgnoreExceptions<TResult>(Func<TResult> func)
            where TResult : class
        {
            TResult result = null;
            try
            {
                result = func();
            }
            catch
            {
            }
            return result;
        }

        /// <summary>
        /// Wrap function call. On exceptions, print exception message to the log and optionally set verdict
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="func">Function to wrap</param>
        /// <param name="func">Pass exception to caller</param>
        /// <param name="upgradeVerdictOnExceptions">Set verdict on errors</param>
        /// <returns>result on success and null on failure</returns>
        protected TResult PrintExceptions<TResult>(Func<TResult> func, bool rethrowException = true, bool upgradeVerdictOnExceptions = true)
            where TResult : class
        {
            TResult result = null;
            try
            {
                result = func();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);

                if (upgradeVerdictOnExceptions)
                    UpgradeVerdict(Verdict.Error);

                if (rethrowException)
                    throw;
            }
            return result;
        }

        protected bool PrintExceptions(Func<bool> func, bool rethrowException = true, bool upgradeVerdictOnExceptions = true)
        {
            bool success = false;
            try
            {
                success = func();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);

                if (upgradeVerdictOnExceptions)
                    UpgradeVerdict(Verdict.Error);

                if (rethrowException)
                    throw;
            }
            return success;
        }

        /// <summary>
        /// Wrap function call. On exceptions, print exception message to the log and optionally set verdict
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="func">Function to wrap</param>
        /// <param name="func">Pass exception to caller</param>
        /// <param name="upgradeVerdictOnExceptions">Set verdict on errors</param>
        protected void PrintExceptions(Action func, bool rethrowException = true, bool upgradeVerdictOnExceptions = true)
        {
            try
            {
                func();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);

                if (upgradeVerdictOnExceptions)
                    UpgradeVerdict(Verdict.Error);

                if (rethrowException)
                    throw;
            }
        }

        protected static byte[] ReadFileContent(string url)
        {
            byte[] content = null;
            try
            {
                content = File.ReadAllBytes(url);
            }
            catch (Exception ex)
            {
                throw new Exception($"Error reading content of {url}: {ex.Message}");
            }
            return content;
        }

        public static string SerializeObject(object toSerialize)
        {
            return JsonConvert.SerializeObject(toSerialize, Formatting.Indented);
        }

        /// <summary>
        /// Call this funciton to update output properties in the UI
        /// </summary>
        public void UpdateOutputProperties()
        {
            // Get properties of self with an Tap Output attribute
            IEnumerable<PropertyInfo> outputProperties = this.GetType().GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(OutputAttribute)));

            // Update all Output properties
            foreach (PropertyInfo prop in outputProperties)
            {
                OnPropertyChanged(prop.Name);
            }
        }

        #endregion

        public YardstickTestStep()
        {
            Rules.Add(() => Yardstick != null, "A Yardstick Instrument Service is required.", "Yardstick");
        }

        public override void PrePlanRun()
        {
            base.PrePlanRun();
        }

        public override void PostPlanRun()
        {
            base.PostPlanRun();

            UpdateOutputProperties();
        }
    }
}

