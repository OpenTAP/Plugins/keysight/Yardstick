﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenTap.Plugins.Yardstick
{
    public interface ISuggestTestSuiteStep
    {
        IEnumerable<string> TSNames { get; }

        string TestSuiteName { get; set; }
    }
    public abstract class SuggestTestSuitesStep : YardstickTestStep, ISuggestTestSuiteStep
    {
        public IEnumerable<string> TSNames { get; protected set; }

        [Display(Name: "Test Suite Name", Group: "Settings")]
        [SuggestedValues("TSNames")]
        public string TestSuiteName { get; set; }

        protected void UpdateTestSuites()
        {
            Task.Factory.StartNew(() => IgnoreExceptions<List<string>>(() => Yardstick.GetTestSuiteList(false)))
            .ContinueWith(result =>
            {
                if (result.Result == null)
                    return;

                TSNames = result.Result.Select((fileName) =>
                {
                    int index = fileName.LastIndexOf('.');
                    return fileName.Substring(0, index);
                });
                this.OnPropertyChanged("TSNames");
            }, TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        public SuggestTestSuitesStep()
        {
            //TSNames = Yardstick.AvailableTestSuites;
            Rules.Add(() => !string.IsNullOrEmpty(TestSuiteName), "A Test Suite Name is required.", "TestSuiteName");

            UpdateTestSuites();
        }
    }

}
