﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using System.Diagnostics;

namespace OpenTap.Plugins.Yardstick.RestClients
{
    public class RestClientResult<TResult>
    {
        #region Properties

        /// <summary>
        /// A text message accompanying the Result. Typically it will be the content sent by the server, f.ex.:
        /// {
        ///   "code": 400,
        ///   "title": "Bad Request",
        ///   "explanation": "The server could not comply with the request since it is either malformed or otherwise incorrect.",
        ///   "error":
        ///   {
        ///     "type": "StackValidationFailed",
        ///     "traceback": null,
        ///     "message": "Property error: : resources.dummy_server.properties.image: : Error validating value 'cirros-0.3.5': No images matching {'name': 'cirros-0.3.5'}."
        ///   }
        /// }
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// Gets the result value of this <see cref="ClientResponse">ClientResponse</see>.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public TResult Result { get; }

        #endregion Properties

        #region ctor

        /// <summary>
        /// Creates a new instance of <see cref="ClientResponse">ClientResponse</see>
        /// </summary>
        /// <param name="message">A text message accompanying the result.</param>
        /// <param name="result">The actual result.</param>
        public RestClientResult(string message, TResult result)
        {


            Message = message;
            Result = result;
        }

        #endregion ctor
    }
}
