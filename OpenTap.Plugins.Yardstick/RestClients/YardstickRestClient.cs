﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenTap.Plugins.Yardstick.Objects;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Settings = OpenTap.Plugins.Yardstick.YardstickSettings;

namespace OpenTap.Plugins.Yardstick.RestClients
{
    /// <summary>
    /// Provides a minimal client for Yardstick REST Server.
    /// </summary>
    internal class YardstickRestClient
    {
        #region Helper functions

        //private static RestClientResult<TOut> V2Deserialize<TOut>(string json)
        //    where TOut : class, new()
        //{
        //    string message = null;
        //    TOut result = null;
        //    V2ResponseStatus responseStatus = JsonConvert.DeserializeObject<V2ResponseStatus>(json);

        //    if (responseStatus == null)
        //    {
        //        message = $"Could not deserialize: {json}";
        //    }

        //    if (responseStatus.Status == 1)
        //    {
        //        V2Response<TOut> successResponse = JsonConvert.DeserializeObject<V2Response<TOut>>(json);
        //        if (successResponse == null)
        //        {
        //            message = $"Could not deserialize: {json}";
        //        }
        //        result = successResponse.Result;
        //    }
        //    else
        //    {
        //        // Operation failed
        //        message = $"Status is not 1 {json}";
        //    }

        //    return new RestClientResult<TOut>(message, result);
        //}

        private static async Task<RestClientResult<TOut>> V2GetAsync<TOut>(string url)
            where TOut : class, new()
        {
            string message = null;
            TOut result = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync(url);
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            // Request was successful.
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            {
                                string json = streamReader.ReadToEnd();
                                V2ResponseStatus response = JsonConvert.DeserializeObject<V2ResponseStatus>(json);

                                if (response == null)
                                {
                                    message = $"Could not deserialize: {responseMessage.Content.ReadAsStringAsync().Result}";
                                }

                                if (response.Status == 1)
                                {
                                    // success, we should be able to parse the result
                                    V2Response<TOut> successResponse = JsonConvert.DeserializeObject<V2Response<TOut>>(json);
                                    if (response == null)
                                    {
                                        message = $"Could not deserialize: {responseMessage.Content.ReadAsStringAsync().Result}";
                                    }
                                    result = successResponse.Result;
                                }
                                else
                                {
                                    // Operation failed
                                    message = $"Status is not 1 {responseMessage.Content.ReadAsStringAsync().Result}";
                                }

                            }
                            break;
                        default:
                            // Unexpected API behavior
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<TOut>(message, result);
        }

        private static async Task<RestClientResult<TOut>> V2PutAsync<TOut>(string url, JObject o)
            where TOut : class, new()
        {
            string message = null;
            TOut result = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                using (StringContent stringContent = new StringContent(o.ToString(), System.Text.Encoding.UTF8, "application/json"))
                {
                    try
                    {
                        HttpResponseMessage responseMessage = await httpClient.PutAsync(url, stringContent);
                        switch (responseMessage.StatusCode)
                        {
                            case HttpStatusCode.OK:
                            case HttpStatusCode.Created:
                                // Request was successful.
                                using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                                using (StreamReader streamReader = new StreamReader(stream))
                                {
                                    string json = streamReader.ReadToEnd();
                                    V2ResponseStatus response = JsonConvert.DeserializeObject<V2ResponseStatus>(json);

                                    if (response == null)
                                    {
                                        message = $"Could not deserialize: {responseMessage.Content.ReadAsStringAsync().Result}";
                                    }

                                    if (response.Status == 1)
                                    {
                                        // success, we should be able to parse the result
                                        V2Response<TOut> successResponse = JsonConvert.DeserializeObject<V2Response<TOut>>(json);
                                        if (response == null)
                                        {
                                            message = $"Could not deserialize: {responseMessage.Content.ReadAsStringAsync().Result}";
                                        }
                                        result = successResponse.Result;
                                    }
                                    else
                                    {
                                        // Operation failed
                                        message = $"Status is not 1 {responseMessage.Content.ReadAsStringAsync().Result}";
                                    }

                                }
                                break;
                            default:
                                // Unexpected API behavior
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                        }
                    }
                    catch (System.OperationCanceledException)
                    {
                        message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                    }
                }
            }

            return new RestClientResult<TOut>(message, result);
        }

        private static async Task<RestClientResult<TOut>> V2PostAsync<TOut>(string url, JObject o)
            where TOut : class, new()
        {
            string message = null;
            TOut result = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                using (StringContent stringContent = new StringContent(o.ToString(), System.Text.Encoding.UTF8, "application/json"))
                {
                    try
                    {
                        HttpResponseMessage responseMessage = await httpClient.PostAsync(url, stringContent);
                        switch (responseMessage.StatusCode)
                        {
                            case HttpStatusCode.OK:
                            case HttpStatusCode.Created:
                                // Request was successful.
                                using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                                using (StreamReader streamReader = new StreamReader(stream))
                                //using (JsonReader jsonReader = new JsonTextReader(streamReader))
                                {
                                    //JsonSerializer serializer = new JsonSerializer();
                                    //V2Response<TOut> response = serializer.Deserialize<V2Response<TOut>>(jsonReader);
                                    //V2Response2 response = serializer.Deserialize<V2Response2>(jsonReader);

                                    string json = streamReader.ReadToEnd();
                                    V2ResponseStatus response = JsonConvert.DeserializeObject<V2ResponseStatus>(json);

                                    if (response == null)
                                    {
                                        message = $"Could not deserialize: {responseMessage.Content.ReadAsStringAsync().Result}";
                                    }

                                    if (response.Status == 1)
                                    {
                                        // success, we should be able to parse the result
                                        V2Response<TOut> successResponse = JsonConvert.DeserializeObject<V2Response<TOut>>(json);
                                        if (response == null)
                                        {
                                            message = $"Could not deserialize: {responseMessage.Content.ReadAsStringAsync().Result}";
                                        }
                                        result = successResponse.Result;
                                    }
                                    else
                                    {
                                        // Operation failed
                                        message = $"Status is not 1 {responseMessage.Content.ReadAsStringAsync().Result}";
                                    }

                                }
                                break;
                            default:
                                // Unexpected API behavior
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                        }
                    }
                    catch (System.OperationCanceledException)
                    {
                        message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                    }
                }
            }

            return new RestClientResult<TOut>(message, result);
        }

        private static async Task<RestClientResult<TOut>> V2DeleteAsync<TOut>(string url)
            where TOut : class, new()
        {
            string message = null;
            TOut result = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                try
                {
                    HttpResponseMessage responseMessage = await httpClient.DeleteAsync(url);
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            // Request was successful.
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            //using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            {
                                // upon successfull execution, a response like following will be retrieved:
                                // {
                                //     "result":
                                //     {
                                //         "project|task|???": "1ad18224-5fd8-47f1-9520-6be29e1e8f42"
                                //     },
                                //     "status": 1
                                // }
                                // where the returned project uuid should match the uuid in the request

                                string json = streamReader.ReadToEnd();
                                V2ResponseStatus response = JsonConvert.DeserializeObject<V2ResponseStatus>(json);

                                if (response == null)
                                {
                                    message = $"Could not deserialize: {responseMessage.Content.ReadAsStringAsync().Result}";
                                }

                                if (response.Status == 1)
                                {
                                    // success, we should be able to parse the result
                                    V2Response<TOut> successResponse = JsonConvert.DeserializeObject<V2Response<TOut>>(json);
                                    if (response == null)
                                    {
                                        message = $"Could not deserialize: {responseMessage.Content.ReadAsStringAsync().Result}";
                                    }
                                    result = successResponse.Result;
                                }
                                else
                                {
                                    // Operation failed
                                    message = $"Status is not 1 {responseMessage.Content.ReadAsStringAsync().Result}";
                                }
                            }
                            break;
                        case HttpStatusCode.BadRequest:
                            // Bad Request -> Some content in the request was invalid.
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            // Unexpected API behavior
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<TOut>(message, result);
        }

        private static async Task<RestClientResult<TOut>> V2UploadAsync<TOut>(string url, MultipartFormDataContent form)
            where TOut : class, new()
        {
            string message = null;
            TOut result = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                try
                {
                    //return new RestClientResult<TOut>("testing", result);
                    HttpResponseMessage responseMessage = await httpClient.PostAsync(url, form);
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                        case HttpStatusCode.Created:
                            // Resource was created and is ready to use.
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            {
                                string json = streamReader.ReadToEnd();
                                V2ResponseStatus response = JsonConvert.DeserializeObject<V2ResponseStatus>(json);

                                if (response == null)
                                {
                                    message = $"Could not deserialize: {responseMessage.Content.ReadAsStringAsync().Result}";
                                }

                                if (response.Status == 1)
                                {
                                    // success, we should be able to parse the result
                                    V2Response<TOut> successResponse = JsonConvert.DeserializeObject<V2Response<TOut>>(json);
                                    if (response == null)
                                    {
                                        message = $"Could not deserialize: {responseMessage.Content.ReadAsStringAsync().Result}";
                                    }
                                    result = successResponse.Result;
                                }
                                else
                                {
                                    // Operation failed
                                    message = $"Status is not 1 {responseMessage.Content.ReadAsStringAsync().Result}";
                                }
                            }

                            break;
                        case HttpStatusCode.BadRequest:
                            // Bad Request -> Some content in the request was invalid.
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.Unauthorized:
                            // Unauthorized -> User must authenticate before making a request.
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.Forbidden:
                            // Forbidden -> Policy does not allow current user to do this operation.
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        case HttpStatusCode.NotFound:
                            // Not Found -> The requested resource could not be found.
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            // Unexpected API behavior
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<TOut>(message, result);
        }

        #endregion

        #region v1 API

        public static async Task<RestClientResult<bool>> IsVersion1APISupported(string baseUrl)
        {
            // There is not direct way to check if the interface is supported, but if it can be queried for results
            // we presume is up and running.

            string message = null;
            bool exists = false;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync($"{baseUrl}/yardstick/results?task_id=dummy");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            // Request was successful.
                            exists = true;
                            break;
                        default:
                            // Unexpected API behavior
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<bool>(message, exists);
        }

        /// <summary>
        /// Get Results for given Task.  This API is used to get the test results of certain task. If you call 
        /// /yardstick/testcases/samples/action API, it will return a task id. You can use the returned task id to get
        /// the results by using this API.
        /// </summary>
        /// <param name="baseUrl">The Yardstick REST Server Base Url.</param>
        /// <param name="taskId">The task Id for which results should be retrieved.</param>
        /// <returns>The list of results for the given task.</returns>
        public static async Task<RestClientResult<string>> GetResult(string baseUrl, string taskId)
        {
            // https://docs.opnfv.org/en/stable-danube/submodules/yardstick/docs/testing/user/userguide/08-api.html

            string message = null;
            string result = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync($"{baseUrl}/yardstick/results?task_id={taskId}");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            // Request was successful.
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            {
                                result = streamReader.ReadToEnd();
                            }
                            break;
                        case HttpStatusCode.BadRequest:
                            // Bad Request -> Some content in the request was invalid.
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            // Unexpected API behavior
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<string>(message, result);
        }

        /// <summary>
        /// Get a list of release test cases.
        /// 
        /// This API is used to list all release test cases now in yardstick.
        /// </summary>
        /// <param name="baseUrl">The Yardstick REST Server Base Url.</param>
        /// <returns>The list of results for the given task.</returns>
        public static async Task<RestClientResult<List<TestCase>>> V1GetTestCaseList(string baseUrl)
        {
            // todo: currently not working, since testcases are not surrounded by an object (result is it just a list)
            string url = $"{baseUrl}/yardstick/testcases";

            return await V2GetAsync<List<TestCase>>(url);
        }

        /// <summary>
        /// Runs the Test Case with the given name
        /// </summary>
        /// <param name="testCaseName">The name of the test case to be run.</param>
        /// <param name="testCaseStage">The stage of the test case to run (sample or release).</param>
        /// <returns>Returns a Guid which is the id of the task which executes the test case.</returns>
        public static async Task<RestClientResult<V1V2RunTestCaseResult>> RunTestCase(string baseUrl, string testCaseName, string testCaseStage)
        {
            // https://docs.opnfv.org/en/stable-danube/submodules/yardstick/docs/testing/user/userguide/08-api.html
            // Todo: add options as input

            string stage = testCaseStage.ToLower();
            Debug.Assert(stage == "release" || stage == "sample");

            string url = $"{baseUrl}/yardstick/testcases/{stage}/action";

            JObject o = JObject.FromObject(new
            {
                action = "run_test_case",
                args = new
                {
                    opts = new { },
                    testcase = testCaseName
                }
            });

            return await V2PostAsync<V1V2RunTestCaseResult>(url, o);

            //string message = null;
            //string taskId = null;

            //using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            //{
            //    JObject o = JObject.FromObject(new
            //    {
            //        action = "run_test_case",
            //        args = new
            //        {
            //            opts = new { },
            //            testcase = testCaseName
            //        }
            //    });

            //    using (StringContent stringContent = new StringContent(o.ToString(), System.Text.Encoding.UTF8, "application/json"))
            //    {
            //        //string testCaseType = (testCaseName.Length == 5 && testCaseName.StartsWith("tc")) ? "release" : "samples";
            //        string testCaseType = "release";

            //        try
            //        {
            //            HttpResponseMessage responseMessage = await httpClient.PostAsync($"{baseUrl}/yardstick/testcases/{testCaseType}/action", stringContent);
            //            switch (responseMessage.StatusCode)
            //            {
            //                case HttpStatusCode.OK:
            //                case HttpStatusCode.Created:
            //                    // Resource was created and is ready to use.

            //                    //token = responseMessage.Headers.GetValues("X-Subject-Token").FirstOrDefault();

            //                    using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
            //                    using (StreamReader streamReader = new StreamReader(stream))
            //                    using (JsonReader jsonReader = new JsonTextReader(streamReader))
            //                    {
            //                        JsonSerializer serializer = new JsonSerializer();
            //                        TaskCreatedResult tcl = serializer.Deserialize<TaskCreatedResult>(jsonReader);

            //                        if (tcl != null && tcl.Result != null)
            //                        {
            //                            taskId = tcl.Result.Id;
            //                        }
            //                        else
            //                        {
            //                            message = $"Could not deserialize {responseMessage.Content.ReadAsStringAsync().Result}";
            //                        }
            //                        //taskId = Guid.Parse(responseMessage.Headers.GetValues("TaskId").FirstOrDefault());
            //                        //taskId = responseMessage.Headers.GetValues("TaskId").FirstOrDefault();
            //                    }

            //                    break;
            //                case HttpStatusCode.BadRequest:
            //                    // Bad Request -> Some content in the request was invalid.
            //                    message = responseMessage.Content.ReadAsStringAsync().Result;
            //                    break;
            //                case HttpStatusCode.Unauthorized:
            //                    // Unauthorized -> User must authenticate before making a request.
            //                    message = responseMessage.Content.ReadAsStringAsync().Result;
            //                    break;
            //                case HttpStatusCode.Forbidden:
            //                    // Forbidden -> Policy does not allow current user to do this operation.
            //                    message = responseMessage.Content.ReadAsStringAsync().Result;
            //                    break;
            //                case HttpStatusCode.NotFound:
            //                    // Not Found -> The requested resource could not be found.
            //                    message = responseMessage.Content.ReadAsStringAsync().Result;
            //                    break;
            //                default:
            //                    // Unexpected API behavior
            //                    message = responseMessage.Content.ReadAsStringAsync().Result;
            //                    break;
            //            }
            //        }
            //        catch (System.OperationCanceledException)
            //        {
            //            message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
            //        }
            //    }

            //}

            //return new RestClientResult<string>(message, taskId);
        }

        /// <summary>
        /// Gets the log output from the Test Case. This is a snapshot reflection, meaning that if the 
        /// test case is not ready executing, the log output will not probably be in its final version.
        /// </summary>
        /// <param name="taskId">The id of the task in which Test Case is running.</param>
        /// <returns>Returns a string representing the log content outputted by the test case.</returns>
        public static async Task<RestClientResult<List<string>>> GetTaskLog(string baseUrl, string taskId)
        {
            // https://docs.opnfv.org/en/stable-danube/submodules/yardstick/docs/testing/user/userguide/08-api.html

            string message = null;
            List<string> lines = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync($"{baseUrl}/yardstick/tasks/{taskId}/log");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            // Request was successful.
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                LogResult lr = serializer.Deserialize<LogResult>(jsonReader);

                                // lr.Status == 0 -> running
                                // lr.Status == 1 -> success
                                // lr.Status == 1 -> fail
                                if (lr.LogData != null && lr.LogData.Data != null)
                                {
                                    lines = lr.LogData.Data;
                                }
                                else
                                {
                                    // log
                                    message = $"Could not deserialize {responseMessage.Content.ReadAsStringAsync().Result}";
                                }
                            }
                            break;
                        case HttpStatusCode.BadRequest:
                            // Bad Request -> Some content in the request was invalid.
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            // Unexpected API behavior
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<List<string>>(message, lines);
        }

        /// <summary>
        /// Gets the status of the task given by taskId.
        /// </summary>
        /// <param name="taskId">The id of the task in which Test Case is running.</param>
        /// <returns>An integeter representing the status. 0->running, 1->pass, 2->fail./returns>
        public static async Task<RestClientResult<int>> GetTaskStatus(string baseUrl, string taskId)
        {
            // https://docs.opnfv.org/en/stable-danube/submodules/yardstick/docs/testing/user/userguide/08-api.html

            string message = null;
            int status = -1;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync($"{baseUrl}/yardstick/results?task_id={taskId}");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            // Request was successful.
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            //using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            {
                                var responseContent = streamReader.ReadToEnd();
                                JObject jobject = JObject.Parse(responseContent);
                                if(jobject.ContainsKey("result") && jobject["result"].Type == JTokenType.String)
                                {
                                    string innerMessage = jobject["result"].ToString();
                                    message = $"Job is in an undeterminated state, defaulting to running. Details: {innerMessage}";
                                    status = 0;
                                }
                                else
                                {
                                    try
                                    {
                                        Objects.TaskStatus taskStatus = JsonConvert.DeserializeObject<Objects.TaskStatus>(responseContent);

                                        // lr.Status == 0 -> running
                                        // lr.Status == 1 -> success
                                        // lr.Status == 2 -> fail
                                        if (taskStatus != null)
                                        {
                                            status = taskStatus.Status;
                                        }
                                        else
                                        {
                                            // log
                                            message = $"Could not deserialize {responseMessage.Content.ReadAsStringAsync().Result}";
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new Exception($"Could not deserialize TaskStatus. Details: {ex.Message}. Content: {responseContent}. taskid: {taskId}. baseurl: {baseUrl}");
                                    }
                                }
                            }
                            break;
                        case HttpStatusCode.BadRequest:
                            // Bad Request -> Some content in the request was invalid.
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            // Unexpected API behavior
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<int>(message, status);
        }

        /// <summary>
        /// Prepare Yardstick test environment
        /// </summary>
        /// <param name="baseUrl">The Yardstick REST Server Base Url.</param>
        /// <param name="action">The action to perform</param>
        /// <returns>Returns a Guid which is the id of the task which prepare the environment</returns>
        public static async Task<RestClientResult<string>> PrepareEnvironment(string baseUrl, PrepareEnvironmentAction action)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html#api-v2-yardstick-tasks
            // Prepare Yardstick test environment
            // {
            //     'action': 'prepare_env|create_grafana|create_influxdb'
            // }

            string message = null;
            string taskId = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                JObject o = new JObject();

                // Set action in json object
                switch (action)
                {
                    case PrepareEnvironmentAction.PrepareEnv:
                        o.Add("action", "prepare_env");
                        break;
                    case PrepareEnvironmentAction.CreateGrafana:
                        o.Add("action", "create_grafana");
                        break;
                    case PrepareEnvironmentAction.CreateInfluxDB:
                        o.Add("action", "create_influxdb");
                        break;
                    default:
                        throw new System.Exception("Unknown action type");
                };

                using (StringContent stringContent = new StringContent(o.ToString(), System.Text.Encoding.UTF8, "application/json"))
                {
                    try
                    {
                        HttpResponseMessage responseMessage = await httpClient.PostAsync($"{baseUrl}/yardstick/env/action", stringContent);
                        switch (responseMessage.StatusCode)
                        {
                            case HttpStatusCode.OK:
                            case HttpStatusCode.Created:
                                // Resource was created and is ready to use.

                                using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                                using (StreamReader streamReader = new StreamReader(stream))
                                using (JsonReader jsonReader = new JsonTextReader(streamReader))
                                {
                                    JsonSerializer serializer = new JsonSerializer();
                                    TaskCreatedResult tcl = serializer.Deserialize<TaskCreatedResult>(jsonReader);

                                    if (tcl != null && tcl.Result != null)
                                    {
                                        taskId = tcl.Result.Id;
                                    }
                                    else
                                    {
                                        message = $"Could not deserialize {responseMessage.Content.ReadAsStringAsync().Result}";
                                    }
                                }

                                break;
                            case HttpStatusCode.BadRequest:
                                // Bad Request -> Some content in the request was invalid.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            case HttpStatusCode.Unauthorized:
                                // Unauthorized -> User must authenticate before making a request.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            case HttpStatusCode.Forbidden:
                                // Forbidden -> Policy does not allow current user to do this operation.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            case HttpStatusCode.NotFound:
                                // Not Found -> The requested resource could not be found.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            default:
                                // Unexpected API behavior
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                        }
                    }
                    catch (System.OperationCanceledException)
                    {
                        message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                    }
                }

            }

            return new RestClientResult<string>(message, taskId);
        }


        /// <summary>
        /// Runs the Test Suite with the given name
        /// </summary>
        /// <param name="baseUrl">The Yardstick REST Server Base Url.</param>
        /// <param name="testSuiteName">The name of the test suite to be run.</param>
        /// <returns>Returns a Guid which is the id of the task which executes the test suite.</returns>
        public static async Task<RestClientResult<string>> RunTestSuite(string baseUrl, string testSuiteName)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html#yardstick-testsuites-action

            // Run a test suite Example:
            // {
            //     'action': 'run_test_suite',
            //     'args': {
            //         'opts': { },
            //         'testsuite': 'opnfv_smoke'
            //     }
            // }

            string message = null;
            string taskId = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                JObject o = JObject.FromObject(new
                {
                    action = "run_test_suite",
                    args = new
                    {
                        opts = new { },
                        testsuite = testSuiteName
                    }
                });

                using (StringContent stringContent = new StringContent(o.ToString(), System.Text.Encoding.UTF8, "application/json"))
                {
                    try
                    {
                        HttpResponseMessage responseMessage = await httpClient.PostAsync($"{baseUrl}/yardstick/testsuites/action", stringContent);
                        switch (responseMessage.StatusCode)
                        {
                            case HttpStatusCode.OK:
                            case HttpStatusCode.Created:
                                // Resource was created and is ready to use.

                                using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                                using (StreamReader streamReader = new StreamReader(stream))
                                using (JsonReader jsonReader = new JsonTextReader(streamReader))
                                {
                                    JsonSerializer serializer = new JsonSerializer();
                                    TaskCreatedResult tcl = serializer.Deserialize<TaskCreatedResult>(jsonReader);

                                    if (tcl != null && tcl.Result != null)
                                    {
                                        taskId = tcl.Result.Id;
                                    }
                                    else
                                    {
                                        message = $"Could not deserialize {responseMessage.Content.ReadAsStringAsync().Result}";
                                    }
                                }

                                break;
                            case HttpStatusCode.BadRequest:
                                // Bad Request -> Some content in the request was invalid.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            case HttpStatusCode.Unauthorized:
                                // Unauthorized -> User must authenticate before making a request.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            case HttpStatusCode.Forbidden:
                                // Forbidden -> Policy does not allow current user to do this operation.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            case HttpStatusCode.NotFound:
                                // Not Found -> The requested resource could not be found.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            default:
                                // Unexpected API behavior
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                        }
                    }
                    catch (System.OperationCanceledException)
                    {
                        message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                    }
                }

            }

            return new RestClientResult<string>(message, taskId);
        }

        #endregion

        #region v2 API

        public static async Task<RestClientResult<bool>> IsVersion2APISupported(string baseUrl)
        {
            // There is not direct way to check if the interface is supported, but if it can be queried for tasks
            // we presume is up and running.

            string message = null;
            bool exists = false;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync($"{baseUrl}/api/v2/yardstick/tasks");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            // Request was successful.
                            exists = true;
                            break;
                        default:
                            // Unexpected API behavior
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<bool>(message, exists);
        }

        #region environment related

        /// <summary>
        /// Creates a Yardstick task        
        /// <param name="baseUrl">The Yardstick REST Server Base Url.</param>
        /// <param name="environmentName"></param>
        /// <returns></returns>
        public static async Task<RestClientResult<string>> CreateEnvironment(string baseUrl, string environmentName)
        {
            // This method is not documented, I have reverse-engineered it from the python code.

            // Create a Yardstick environment;
            // 
            // METHOD: POST
            // 
            // Create a Yardstick task Example:
            // 
            // {
            //   "action": "create_environment",
            //   "args":
            //   {
            //     "name": "My newest environment"
            //   }
            // }

            string message = null;
            string environmentId = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                JObject o = JObject.FromObject(new
                {
                    action = "create_environment",
                    args = new
                    {
                        name = environmentName
                    }
                });

                using (StringContent stringContent = new StringContent(o.ToString(), System.Text.Encoding.UTF8, "application/json"))
                {
                    try
                    {
                        HttpResponseMessage responseMessage = await httpClient.PostAsync($"{baseUrl}/api/v2/yardstick/environments", stringContent);
                        switch (responseMessage.StatusCode)
                        {
                            case HttpStatusCode.OK:
                            case HttpStatusCode.Created:
                                // Resource was created and is ready to use.
                                //token = responseMessage.Headers.GetValues("X-Subject-Token").FirstOrDefault();
                                using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                                using (StreamReader streamReader = new StreamReader(stream))
                                using (JsonReader jsonReader = new JsonTextReader(streamReader))
                                {
                                    JsonSerializer serializer = new JsonSerializer();
                                    EnvironmentCreatedResult ecr = serializer.Deserialize<EnvironmentCreatedResult>(jsonReader);

                                    // The result should be something like this:
                                    //
                                    // {
                                    //   "result":
                                    //   {
                                    //     "uuid": "1819eb72-c10f-44c8-a3b7-94391a8c16e2"
                                    //   },
                                    //   "status": 1
                                    // }
                                    if (ecr != null && ecr.Environment != null)
                                    {
                                        if (ecr.Status == 1)
                                        {
                                            environmentId = ecr.Environment.UUID;
                                        }
                                        else
                                        {
                                            // log
                                            message = $"Status is not 1 {responseMessage.Content.ReadAsStringAsync().Result}";
                                        }
                                    }
                                    else
                                    {
                                        // log
                                        message = $"Could not deserialize: {responseMessage.Content.ReadAsStringAsync().Result}";
                                    }
                                }

                                break;
                            case HttpStatusCode.BadRequest:
                                // Bad Request -> Some content in the request was invalid.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            case HttpStatusCode.Unauthorized:
                                // Unauthorized -> User must authenticate before making a request.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            case HttpStatusCode.Forbidden:
                                // Forbidden -> Policy does not allow current user to do this operation.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            case HttpStatusCode.NotFound:
                                // Not Found -> The requested resource could not be found.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            default:
                                // Unexpected API behavior
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                        }
                    }
                    catch (System.OperationCanceledException)
                    {
                        message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                    }
                }
            }

            return new RestClientResult<string>(message, environmentId);
        }

        /// <summary>
        /// Creates a Yardstick task        
        /// <param name="baseUrl">The Yardstick REST Server Base Url.</param>
        /// <param name="environmentName"></param>
        /// <returns></returns>
        public static async Task<RestClientResult<V2DeletedEnvironmentResult>> DeleteEnvironment(string baseUrl, string environmentId)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            string url = $"{baseUrl}/api/v2/yardstick/environments/{environmentId}";

            return await V2DeleteAsync<V2DeletedEnvironmentResult>(url);

            // This method is not documented, I have reverse-engineered it from the python code.

            // Delete a Yardstick environment;
            // 
            // METHOD: DELETE

            //string message = null;
            //bool deleted = false;

            //using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            //{
            //    try
            //    {
            //        HttpResponseMessage responseMessage = await httpClient.DeleteAsync($"{baseUrl}/api/v2/yardstick/environments/{environmentId}");
            //        switch (responseMessage.StatusCode)
            //        {
            //            case HttpStatusCode.OK:
            //                // Resource was created and is ready to use.
            //                //token = responseMessage.Headers.GetValues("X-Subject-Token").FirstOrDefault();
            //                using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
            //                using (StreamReader streamReader = new StreamReader(stream))
            //                using (JsonReader jsonReader = new JsonTextReader(streamReader))
            //                {
            //                    JsonSerializer serializer = new JsonSerializer();
            //                    EnvironmentDeletedResult edr = serializer.Deserialize<EnvironmentDeletedResult>(jsonReader);

            //                    // The result should be something like this:
            //                    //
            //                    // {
            //                    //   "result":
            //                    //   {
            //                    //     "uuid": "1819eb72-c10f-44c8-a3b7-94391a8c16e2"
            //                    //   },
            //                    //   "status": 1
            //                    // }

            //                    if (edr != null && edr.Environment != null)
            //                    {
            //                        if (edr.Status == 1)
            //                        {
            //                            if(environmentId == edr.Environment.UUID)
            //                            {
            //                                deleted = true;
            //                            }
            //                            else
            //                            {
            //                                // log
            //                                message = $"Another environment has been deleted: {edr.Environment.UUID}";
            //                            }
            //                        }
            //                        else
            //                        {
            //                            // log
            //                            message = $"Status is not 1 {responseMessage.Content.ReadAsStringAsync().Result}";
            //                        }
            //                    }
            //                    else
            //                    {
            //                        // log
            //                        message = $"Could not deserialize: {responseMessage.Content.ReadAsStringAsync().Result}";
            //                    }
            //                }

            //                break;
            //            case HttpStatusCode.BadRequest:
            //                // Bad Request -> Some content in the request was invalid.
            //                message = responseMessage.Content.ReadAsStringAsync().Result;
            //                break;
            //            case HttpStatusCode.Unauthorized:
            //                // Unauthorized -> User must authenticate before making a request.
            //                message = responseMessage.Content.ReadAsStringAsync().Result;
            //                break;
            //            case HttpStatusCode.Forbidden:
            //                // Forbidden -> Policy does not allow current user to do this operation.
            //                message = responseMessage.Content.ReadAsStringAsync().Result;
            //                break;
            //            case HttpStatusCode.NotFound:
            //                // Not Found -> The requested resource could not be found.
            //                //message = responseMessage.Content.ReadAsStringAsync().Result;
            //                deleted = true; // already deleted
            //                break;
            //            default:
            //                // Unexpected API behavior
            //                message = responseMessage.Content.ReadAsStringAsync().Result;
            //                break;
            //        }
            //    }
            //    catch (System.OperationCanceledException)
            //    {
            //        message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
            //    }
            //}

            //return new RestClientResult<bool>(message, deleted);
        }

        /// <summary>
        /// Get a list of currently available tasks in Yardstick.
        /// </summary>
        /// <param name="baseUrl">The Yardstick REST Server Base Url.</param>
        /// <returns>The list of available tasks.</returns>
        public static async Task<RestClientResult<List<Objects.Environment>>> ListEnvironments(string baseUrl)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            string message = null;
            List<Objects.Environment> environments = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync($"{baseUrl}/api/v2/yardstick/environments");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            // Request was successful.
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                EnvironmentListResult elr = serializer.Deserialize<EnvironmentListResult>(jsonReader);

                                if (elr.Status == 1 && elr.Result != null)
                                {
                                    environments = elr.Result.Environments;
                                }
                                else
                                {
                                    // log
                                    message = $"Status is not 1 {responseMessage.Content.ReadAsStringAsync().Result}";
                                }
                            }
                            break;
                        case HttpStatusCode.BadRequest:
                            // Bad Request -> Some content in the request was invalid.
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            // Unexpected API behavior
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<List<Objects.Environment>>(message, environments);
        }

        public static async Task<RestClientResult<V2OpenrcUploadedResult>> UploadOpenrcFile(string baseUrl, byte[] openrcFileContent, string environmentId)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html#api-v2-yardstick-openrcs

            // Upload openrc file;
            // Which API to call will depend on the parameters.

            // METHOD: POST

            // Upload an openrc file for an OpenStack environment Example:
            // {
            //     'action': 'upload_openrc',
            //     'args': {
            //         'file': file,
            //         'environment_id': environment_id
            //     }
            // }

            string url = $"{baseUrl}/api/v2/yardstick/openrcs";

            HttpContent file = ToBase64HttpContent(openrcFileContent);

            using (MultipartFormDataContent form = new MultipartFormDataContent())
            {
                //var streamContent = new StreamContent(openrcStream);
                form.Add(new StringContent("upload_openrc"), "action");
                form.Add(new StringContent(environmentId), "environment_id");
                form.Add(file, "file", "openrc.sh");

                return await V2UploadAsync<V2OpenrcUploadedResult>(url, form);
            }
        }

        #endregion

        #region Test case related

        /// <summary>
        /// Get a list of release test cases.
        /// 
        /// This API is used to list all release test cases now in yardstick.
        /// </summary>
        /// <param name="baseUrl">The Yardstick REST Server Base Url.</param>
        /// <returns>The list of results for the given task.</returns>
        public static async Task<RestClientResult<V2GetTestCasesResult>> V2GetTestCaseList(string baseUrl)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html#api-v2-yardstick-testcases

            string url = $"{baseUrl}/api/v2/yardstick/testcases";

            return await V2GetAsync<V2GetTestCasesResult>(url);
        }

        public static async Task<RestClientResult<V2DeletedTestCaseResult>> DeleteTestCase(string baseUrl, string testCaseName)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html#api-v2-yardstick-testcases-case-name

            string url = $"{baseUrl}/api/v2/yardstick/testcases/{testCaseName}";

            return await V2DeleteAsync<V2DeletedTestCaseResult>(url);
        }

        private static HttpContent ToBase64HttpContent(byte[] fileContent)
        {
            // Convert file content to base64
            string base64FileContent = System.Convert.ToBase64String(fileContent);
            var file = new StringContent(base64FileContent);
            file.Headers.Add("Content-Transfer-Encoding", "base64");
            return file;
        }

        public static async Task<RestClientResult<V2UploadedTestCaseResult>> UploadTestCaseFile(string baseUrl, byte[] testCaseFileContent, string testCaseName)
        {
            string url = $"{baseUrl}/api/v2/yardstick/testcases";

            string testCaseFileName = testCaseName + ".yaml";

            var file = ToBase64HttpContent(testCaseFileContent);

            using (MultipartFormDataContent form = new MultipartFormDataContent())
            {
                //var streamContent = new StreamContent(testCaseStream);
                form.Add(new StringContent("upload_case"), "action");
                //form.Add(streamContent, "file", testCaseName);

                //var file = new ByteArrayContent(Encoding.ASCII.GetBytes("IyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjDQojIENvcHlyaWdodCAoYykgMjAxNyBFcmljc3NvbiBBQiBhbmQgb3RoZXJzLg0KIw0KIyBBbGwgcmlnaHRzIHJlc2VydmVkLiBUaGlzIHByb2dyYW0gYW5kIHRoZSBhY2NvbXBhbnlpbmcgbWF0ZXJpYWxzDQojIGFyZSBtYWRlIGF2YWlsYWJsZSB1bmRlciB0aGUgdGVybXMgb2YgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMA0KIyB3aGljaCBhY2NvbXBhbmllcyB0aGlzIGRpc3RyaWJ1dGlvbiwgYW5kIGlzIGF2YWlsYWJsZSBhdA0KIyBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjANCiMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIw0KLS0tDQoNCnNjaGVtYTogInlhcmRzdGljazp0YXNrOjAuMSINCmRlc2NyaXB0aW9uOiA+DQogICAgWWFyZHN0aWNrIFRDMDAxIGNvbmZpZyBmaWxlOw0KICAgIE1lYXN1cmUgbmV0d29yayB0aHJvdWdocHV0IHVzaW5nIHBrdGdlbjsNCiAgICBEaWZmZXJlbnQgYW1vdW50cyBvZiBmbG93cyBhcmUgdGVzdGVkIHdpdGgsIGZyb20gMiB1cCB0byAxMDAxMDAwOw0KICAgIEFsbCB0ZXN0cyBhcmUgcnVuIHR3aWNlLiBGaXJzdCB0d2ljZSB3aXRoIHRoZSBsZWFzdCBhbW91bnQgb2YgcG9ydHMgYW5kIGZ1cnRoZXIgb24uDQoNCnslIHNldCBwcm92aWRlciA9IHByb3ZpZGVyIG9yIG5vbmUgJX0NCnslIHNldCBwaHlzaWNhbF9uZXR3b3JrID0gcGh5c2ljYWxfbmV0d29yayBvciAncGh5c25ldDEnICV9DQp7JSBzZXQgc2VnbWVudGF0aW9uX2lkID0gc2VnbWVudGF0aW9uX2lkIG9yIG5vbmUgJX0NCg0Kc2NlbmFyaW9zOg0KeyUgZm9yIG51bV9wb3J0cyBpbiBbMSwgMTAsIDUwLCAxMDAsIDUwMCwgMTAwMF0gJX0NCi0NCiAgdHlwZTogUGt0Z2VuDQogIG9wdGlvbnM6DQogICAgcGFja2V0c2l6ZTogNjQNCiAgICBudW1iZXJfb2ZfcG9ydHM6IHt7bnVtX3BvcnRzfX0NCiAgICBkdXJhdGlvbjogMjANCg0KICBob3N0OiBkZW1ldGVyLnlhcmRzdGljaw0KICB0YXJnZXQ6IHBvc2VpZG9uLnlhcmRzdGljaw0KDQogIHJ1bm5lcjoNCiAgICB0eXBlOiBJdGVyYXRpb24NCiAgICBpdGVyYXRpb25zOiAyDQogICAgaW50ZXJ2YWw6IDENCg0KICBzbGE6DQogICAgbWF4X3BwbTogMTAwMA0KICAgIGFjdGlvbjogbW9uaXRvcg0KeyUgZW5kZm9yICV9DQoNCmNvbnRleHQ6DQogIG5hbWU6IHlhcmRzdGljaw0KICBpbWFnZTogeWFyZHN0aWNrLWltYWdlDQogIGZsYXZvcjogeWFyZHN0aWNrLWZsYXZvcg0KICB1c2VyOiB1YnVudHUNCg0KICBwbGFjZW1lbnRfZ3JvdXBzOg0KICAgIHBncnAxOg0KICAgICAgcG9saWN5OiAiYXZhaWxhYmlsaXR5Ig0KDQogIHNlcnZlcnM6DQogICAgZGVtZXRlcjoNCiAgICAgIGZsb2F0aW5nX2lwOiB0cnVlDQogICAgICBwbGFjZW1lbnQ6ICJwZ3JwMSINCiAgICBwb3NlaWRvbjoNCiAgICAgIGZsb2F0aW5nX2lwOiB0cnVlDQogICAgICBwbGFjZW1lbnQ6ICJwZ3JwMSINCg0KICBuZXR3b3JrczoNCiAgICB0ZXN0Og0KICAgICAgY2lkcjogJzEwLjAuMS4wLzI0Jw0KICAgICAgeyUgaWYgcHJvdmlkZXIgPT0gInZsYW4iIG9yIHByb3ZpZGVyID09ICJzcmlvdiIgJX0NCiAgICAgIHByb3ZpZGVyOiB7e3Byb3ZpZGVyfX0NCiAgICAgIHBoeXNpY2FsX25ldHdvcms6IHt7cGh5c2ljYWxfbmV0d29ya319DQogICAgICAgIHslIGlmIHNlZ21lbnRhdGlvbl9pZCAlfQ0KICAgICAgc2VnbWVudGF0aW9uX2lkOiB7e3NlZ21lbnRhdGlvbl9pZH19DQogICAgICAgIHslIGVuZGlmICV9DQogICAgICB7JSBlbmRpZiAlfQ=="));
                //file.Headers.Add("Content-Type", "multipart/form-data");

                form.Add(file, "file", testCaseFileName);

                return await V2UploadAsync<V2UploadedTestCaseResult>(url, form);
            }
        }

        public static async Task<RestClientResult<V2GetTestCaseResult>> GetTestCase(string baseUrl, string testCaseName)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html#api-v2-yardstick-testcases-case-name

            string url = $"{baseUrl}/api/v2/yardstick/testcases/{testCaseName}";

            return await V2GetAsync<V2GetTestCaseResult>(url);
        }

        #endregion

        #region task related

        /// <summary>
        /// Creates a Yardstick task        
        /// <param name="baseUrl">The Yardstick REST Server Base Url.</param>
        /// <param name="taskName"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public static async Task<RestClientResult<V2CreatedTaskResult>> CreateTask(string baseUrl, string taskName, string projectId)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            // Create a Yardstick task;
            // Which API to call will depend on the parameters.
            // 
            // METHOD: POST
            // 
            // Create a Yardstick task Example:
            // {
            //     'action': 'create_task',
            //         'args': {
            //             'name': 'task1',
            //             'project_id': project_id
            //         }
            // }
            // </summary>
            // 

            string url = $"{baseUrl}/api/v2/yardstick/tasks";

            JObject o = JObject.FromObject(new
            {
                action = "create_task",
                args = new
                {
                    name = taskName,
                    project_id = projectId
                }
            });

            return await V2PostAsync<V2CreatedTaskResult>(url, o);
        }

        public static async Task<RestClientResult<V2DeletedTaskResult>> DeleteTask(string baseUrl, string taskId)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html#api-v2-yardstick-tasks-task-id

            string url = $"{baseUrl}/api/v2/yardstick/tasks/{taskId}";

            return await V2DeleteAsync<V2DeletedTaskResult>(url);
        }

        /// <summary>
        /// Get a list of currently available tasks in Yardstick.
        /// </summary>
        /// <param name="baseUrl">The Yardstick REST Server Base Url.</param>
        /// <returns>The list of available tasks.</returns>
        public static async Task<RestClientResult<List<Objects.Task>>> ListTasks(string baseUrl)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            string message = null;
            List<Objects.Task> tasks = null;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                try
                {
                    HttpResponseMessage responseMessage = await httpClient.GetAsync($"{baseUrl}/yardstick/tasks");
                    switch (responseMessage.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            // Request was successful.
                            using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                            using (StreamReader streamReader = new StreamReader(stream))
                            using (JsonReader jsonReader = new JsonTextReader(streamReader))
                            {
                                JsonSerializer serializer = new JsonSerializer();
                                TaskList tl = serializer.Deserialize<TaskList>(jsonReader);

                                if (tl.Status == 1)
                                {
                                    tasks = tl.Tasks;
                                }
                                else
                                {
                                    // log
                                    message = $"Status is not 1 {responseMessage.Content.ReadAsStringAsync().Result}";
                                }
                            }
                            break;
                        case HttpStatusCode.BadRequest:
                            // Bad Request -> Some content in the request was invalid.
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                        default:
                            // Unexpected API behavior
                            message = responseMessage.Content.ReadAsStringAsync().Result;
                            break;
                    }
                }
                catch (System.OperationCanceledException)
                {
                    message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                }
            }

            return new RestClientResult<List<Objects.Task>>(message, tasks);
        }

        public static async Task<RestClientResult<bool>> AddEnvironmentToTask(string baseUrl, string taskId, string environmentId)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            // /api/v2/yardstick/tasks/<task_id>
            // METHOD: PUT
            // 
            // Add a environment to a task
            // 
            // Example:
            // 
            // {
            //     'action': 'add_environment',
            //     'args':
            //     {
            //         'environment_id': 'e3cadbbb-0419-4fed-96f1-a232daa0422a'
            //     }
            // }

            string message = null;
            bool succeeded = false;

            using (var httpClient = new HttpClient() { Timeout = TimeSpan.FromMilliseconds(Settings.Current.RestCallTimeout) })
            {
                JObject o = JObject.FromObject(new
                {
                    action = "add_environment",
                    args = new
                    {
                        environment_id = environmentId
                    }
                });


                using (StringContent stringContent = new StringContent(o.ToString(), System.Text.Encoding.UTF8, "application/json"))
                {
                    try
                    {
                        HttpResponseMessage responseMessage = await httpClient.PutAsync($"{baseUrl}/api/v2/yardstick/tasks/{taskId}", stringContent);
                        switch (responseMessage.StatusCode)
                        {
                            case HttpStatusCode.OK:
                            case HttpStatusCode.Created:
                                // Resource was created and is ready to use.
                                using (Stream stream = await responseMessage.Content.ReadAsStreamAsync())
                                using (StreamReader streamReader = new StreamReader(stream))
                                using (JsonReader jsonReader = new JsonTextReader(streamReader))
                                {
                                    JsonSerializer serializer = new JsonSerializer();
                                    EnvironmentAddedResult ear = serializer.Deserialize<EnvironmentAddedResult>(jsonReader);

                                    // The result should be something like this:
                                    //
                                    // {
                                    //     "result": {
                                    //         "uuid": "6677f943-1d63-48a8-9f7c-d3542ee78797"
                                    //     },
                                    //     "status": 1
                                    // }

                                    if (ear != null && ear.Result != null)
                                    {
                                        if (ear.Status == 1)
                                        {
                                            succeeded = true;
                                        }
                                        else
                                        {
                                            // log
                                            message = $"Status is not 1 {responseMessage.Content.ReadAsStringAsync().Result}";
                                        }
                                    }
                                    else
                                    {
                                        // log
                                        message = $"Could not deserialize: {responseMessage.Content.ReadAsStringAsync().Result}";
                                    }
                                }

                                break;
                            case HttpStatusCode.BadRequest:
                                // Bad Request -> Some content in the request was invalid.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            case HttpStatusCode.Unauthorized:
                                // Unauthorized -> User must authenticate before making a request.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            case HttpStatusCode.Forbidden:
                                // Forbidden -> Policy does not allow current user to do this operation.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            case HttpStatusCode.NotFound:
                                // Not Found -> The requested resource could not be found.
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                            default:
                                // Unexpected API behavior
                                message = responseMessage.Content.ReadAsStringAsync().Result;
                                break;
                        }
                    }
                    catch (System.OperationCanceledException)
                    {
                        message = $"The communication timed-out after {Settings.Current.RestCallTimeout}ms.";
                    }
                }
            }

            return new RestClientResult<bool>(message, succeeded);
        }

        public static async Task<RestClientResult<V2AddedCaseToTaskResult>> AddTestCaseToTask(string baseUrl, string taskId, string testCaseName, string testCaseContent)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            // /api/v2/yardstick/tasks/<task_id>
            // METHOD: PUT
            // 
            // Add a test case to a task
            // 
            // Example:
            // 
            // {
            //     'action': 'add_case',
            //     'args':
            //     {
            //         'case_name': 'opnfv_yardstick_tc002'
            //         'case_content': case_content
            //     }
            // }

            string url = $"{baseUrl}/api/v2/yardstick/tasks/{taskId}";

            JObject o = JObject.FromObject(new
            {
                action = "add_case",
                args = new
                {
                    case_name = testCaseName,
                    case_content = testCaseContent
                }
            });

            return await V2PutAsync<V2AddedCaseToTaskResult>(url, o);
        }

        public static async Task<RestClientResult<V2AddedSuiteToTaskResult>> AddTestSuiteToTask(string baseUrl, string taskId, string testSuiteName, string testSuiteContent)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            // /api/v2/yardstick/tasks/<task_id>
            // METHOD: PUT
            // 
            // Add a test suite to a task
            // 
            // Example:
            // 
            // {
            //     'action': 'add_suite',
            //     'args':
            //     {
            //         'suite_name': 'opnfv_smoke'
            //         'suite_content': suite_content
            //     }
            // }

            string url = $"{baseUrl}/api/v2/yardstick/tasks/{taskId}";

            JObject o = JObject.FromObject(new
            {
                action = "add_suite",
                args = new
                {
                    suite_name = testSuiteName,
                    suite_content = testSuiteContent
                }
            });

            return await V2PutAsync<V2AddedSuiteToTaskResult>(url, o);
        }

        /// <summary>
        /// Runs the Task with the given id
        /// </summary>
        /// <param name="taskId">The id of the task to run.</param>
        /// <returns>Returns a Guid which is the id of the task which executes the test case.</returns>
        public static async Task<RestClientResult<V2RunTaskResult>> RunTask(string baseUrl, string taskId)
        {

            string url = $"{baseUrl}/api/v2/yardstick/tasks/{taskId}";

            JObject o = JObject.FromObject(new
            {
                action = "run"
            });

            return await V2PutAsync<V2RunTaskResult>(url, o);
        }

        public static async Task<RestClientResult<V2TaskInfoResult>> GetTaskInformation(string baseUrl, string taskId)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            string url = $"{baseUrl}/api/v2/yardstick/tasks/{taskId}";

            return await V2GetAsync<V2TaskInfoResult>(url);
        }

        public static async Task<RestClientResult<V2GetTasksResult>> GetTasks(string baseUrl)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            string url = $"{baseUrl}/api/v2/yardstick/tasks";

            return await V2GetAsync<V2GetTasksResult>(url);
        }

        #endregion

        #region Suite related

        public static async Task<RestClientResult<V2CreatedTestSuiteResult>> CreateTestSuite(string baseUrl, string testSuiteName, List<string> testCaseNames)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html


            // METHOD: POST
            // Create a Yardstick suite Example:
            // {
            //     'action': 'create_suite',
            //     'args': {
            //         'name': < suite_name >,
            //         'testcases': [
            //             'opnfv_yardstick_tc002'
            //         ]
            //     }
            // }

            string url = $"{baseUrl}/api/v2/yardstick/testsuites";

            JObject o = JObject.FromObject(new
            {
                action = "create_suite",
                args = new
                {
                    name = testSuiteName,
                    testcases = testCaseNames
                }
            });

            return await V2PostAsync<V2CreatedTestSuiteResult>(url, o);
        }

        public static async Task<RestClientResult<V2DeletedTestSuiteResult>> DeleteTestSuite(string baseUrl, string testSuiteName)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            string url = $"{baseUrl}/api/v2/yardstick/testsuites/{testSuiteName}";

            return await V2DeleteAsync<V2DeletedTestSuiteResult>(url);
        }

        /// <summary>
        /// Get a list of test suites.
        /// 
        /// This API is used to list all test suites now in yardstick.
        /// </summary>
        /// <param name="baseUrl">The Yardstick REST Server Base Url.</param>
        /// <returns>The list of results for the given task.</returns>
        public static async Task<RestClientResult<V2GetTestSuitesResult>> GetTestSuiteList(string baseUrl)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            string url = $"{baseUrl}/api/v2/yardstick/testsuites";

            return await V2GetAsync<V2GetTestSuitesResult>(url);
        }

        public static async Task<RestClientResult<V2GetTestSuiteResult>> GetTestSuite(string baseUrl, string suiteName)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            string url = $"{baseUrl}/api/v2/yardstick/testsuites/{suiteName}";

            return await V2GetAsync<V2GetTestSuiteResult>(url);
        }

        #endregion

        #region project related

        public static async Task<RestClientResult<V2CreatedProjectResult>> CreateProject(string baseUrl, string projectName)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            // Create a Yardstick project;
            // Get all projects;
            // Which API to call will depend on the parameters.
            // 
            // METHOD: POST
            // 
            // Create a Yardstick project Example:
            // {
            //     'action': 'create_project',
            //     'args':
            //     {
            //         'name': 'project1'
            //     }
            // }

            string url = $"{baseUrl}/api/v2/yardstick/projects";

            JObject o = JObject.FromObject(new
            {
                action = "create_project",
                args = new
                {
                    name = projectName
                }
            });

            return await V2PostAsync<V2CreatedProjectResult>(url, o);
        }

        public static async Task<RestClientResult<V2DeletedProjectResult>> DeleteProject(string baseUrl, string projectId)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            string url = $"{baseUrl}/api/v2/yardstick/projects/{projectId}";

            return await V2DeleteAsync<V2DeletedProjectResult>(url);
        }

        public static async Task<RestClientResult<V2GetProjectsResult>> GetProjects(string baseUrl)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            string url = $"{baseUrl}/api/v2/yardstick/projects";

            return await V2GetAsync<V2GetProjectsResult>(url);
        }

        public static async Task<RestClientResult<V2GetProjectResult>> GetProjectInfo(string baseUrl, string projectId)
        {
            // https://docs.opnfv.org/en/stable-fraser/submodules/yardstick/docs/testing/user/userguide/09-api.html

            string url = $"{baseUrl}/api/v2/yardstick/projects/{projectId}";

            return await V2GetAsync<V2GetProjectResult>(url);
        }

        #endregion

        #endregion
    }
}
