﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using OpenTap.Plugins.Yardstick.Objects;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace OpenTap.Plugins.Yardstick
{
    [Display("List Environments", Groups: new string[] { "Yardstick", "V2 API", "Environment" }, Description: "List Available Environments")]
    public class ListEnvironments : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Environments", Description: "The list of environments that are available in Yardstick.", Group: "Output", Collapsed: true)]
        [Output]
        public List<Environment> Environments { get; set; }

        #endregion

        public ListEnvironments()
        {
            Name = "List Environments";
        }

        public override void Run()
        {
            // Get list of environments
            List<Environment> environments = Yardstick.ListEnvironments();


            if (environments != null)
            {
                Environments = environments;
                // Print list of environments to log
                //foreach (Environment environment in environments)
                //{
                //    Log.Info(environment.ToString());
                //}
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }

    [Display("Create Environment", Groups: new string[] { "Yardstick", "V2 API", "Environment" }, Description: "Create new environment")]
    public class CreateEnvironment : YardstickTestStep
    {
        private string fileContentAsBase64 = null;
        #region Settings

        [Display(Name: "Name", Group: "Settings", Description: "The name of the environment to be created.")]
        public string EnvironmentName { get; set; }

        [FilePath()]
        [Display(Name: "Openrc file", Description: "Open stack credential file.", Group: "Settings")]
        public string FileBase64
        {
            get
            {
                return fileContentAsBase64;
            }
            set
            {
                bool fallback = true;
                FileInfo fi = null;
                try
                {
                    fi = new FileInfo(value);
                    if (fi.Exists)
                    {
                        string fileContent = File.ReadAllText(value);
                        fileContentAsBase64 = Base64Encoding.EncodeBase64(fileContent);
                        fallback = false;
                    }
                }
                catch (System.Exception ex)
                {
                    Log.Error(ex.Message);
                }

                if (fallback)
                {
                    if (Base64Encoding.IsBase64(value))
                    {
                        fileContentAsBase64 = value;
                    }
                    else
                    {
                        fileContentAsBase64 = null;
                    }
                }
            }
        }

        [Display(Name: "Id", Group: "Output", Description: "The id of the created environment.", Collapsed: true)]
        [Output]
        public string EnvironmentId { get; protected set; }

        #endregion

        public CreateEnvironment()
        {
            Name = "Create Environment";
            Rules.Add(() => !string.IsNullOrEmpty(EnvironmentName), "An environment name is required.", "EnvironmentName");
            Rules.Add(() => !string.IsNullOrEmpty(FileBase64), "An openrc file is required.", "FileBase64");
        }

        /// <summary>
        /// Upload openrc file and delete env if it fails
        /// </summary>
        /// <param name="openrcContent"></param>
        /// <returns>Success/failed</returns>
        private bool UploadOpenrcFile(byte[] openrcContent, string environmentId)
        {
            V2OpenrcUploadedResult openrcResult = PrintExceptions<V2OpenrcUploadedResult>(() => Yardstick.UploadOpenrcFile(openrcContent, environmentId), false);
            return openrcResult != null;
        }

        public override void Run()
        {
            // Read OpenRC file content
            byte[] openrcContent = Encoding.UTF8.GetBytes(Base64Encoding.DecodeBase64(fileContentAsBase64));
            Debug.Assert(openrcContent != null);

            // Create environment
            string environmentId = PrintExceptions<string>(() => Yardstick.CreateEnvironment(EnvironmentName));
            Debug.Assert(environmentId != null);

            //if (environmentId != null)
            //{
                // Upload openrc file
                bool uploaded = UploadOpenrcFile(openrcContent, environmentId);
                if (uploaded)
                {
                    // If upload succeeded, then update environment id and upgrade verdict to pass
                    EnvironmentId = environmentId;
                    UpgradeVerdict(Verdict.Pass);
                }
                else
                {
                    // If upload failed, then delete environment
                    PrintExceptions(() => Yardstick.DeleteEnvironment(environmentId));
                }
            //}
            //else
            //{
            //    UpgradeVerdict(Verdict.Fail);
            //}
        }
    }


    [Display("Delete Environment", Groups: new string[] { "Yardstick", "V2 API", "Environment" }, Description: "Delete environment")]
    public class DeleteEnvironment : YardstickTestStep
    {
        #region Settings

        [Display(Name: "Id", Description: "The id of environment to be deleted.", Group: "Settings")]
        public Input<string> EnvironmentId { get; set; }

        #endregion

        public DeleteEnvironment()
        {
            EnvironmentId = new Input<string>();
            Name = "Delete Environment";

            Rules.Add(() => EnvironmentId.Step != null, "An environment id is required.", "EnvironmentId");
        }

        public override void Run()
        {
            bool deleted = Yardstick.DeleteEnvironment(EnvironmentId.Value);

            if (deleted == true)
            {
                UpgradeVerdict(Verdict.Pass);
            }
            else
            {
                UpgradeVerdict(Verdict.Fail);
            }
        }
    }
}