﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Newtonsoft.Json;
using System.Collections.Generic;

namespace OpenTap.Plugins.Yardstick.Objects
{
    [JsonObject("Task")]
    public class Task
    {
        public static object Factory { get; internal set; }
        [JsonProperty("Id")]
        public string Id { get; set; }

        public override string ToString()
        {
            return $"Task Id=[{Id}]";
        }
    }

    [JsonObject("TaskList")]
    public class TaskList
    {
        [JsonProperty("result")]
        public List<Task> Tasks { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    [JsonObject("TaskInfo")]
    public class TaskInfo
    {
        [JsonProperty("deploy_scenario")]
        public string DeployScenario { get; set; }

        [JsonProperty("installer")]
        public string Installer { get; set; }

        [JsonProperty("pod_name")]
        public string PodName { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }
    }

    [JsonObject("TaskStatusResult")]
    public class TaskStatusResult
    {
        [JsonProperty("criteria")]
        public string Criteria { get; set; }

        [JsonProperty("info")]
        public TaskInfo Info { get; set; }

        [JsonProperty("task_id")]
        public string TaskId { get; set; }

        //[JsonProperty("testcases")]
        //public List<TestCaseResult> TestCases { get; set; }
    }

    [JsonObject("TaskStatus")]
    public class TaskStatus
    {
        [JsonProperty("result")]
        public TaskStatusResult Result { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    [JsonObject("TaskCreated")]
    public class TaskCreated
    {
        [JsonProperty("task_id")]
        public string Id { get; set; }
    }

    [JsonObject("TaskCreatedResult")]
    public class TaskCreatedResult
    {
        [JsonProperty("result")]
        public TaskCreated Result { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    public enum PrepareEnvironmentAction
    {
        PrepareEnv,
        CreateGrafana,
        CreateInfluxDB
    }

    [JsonObject("EnvironmentAdded")]
    public class EnvironmentAdded
    {
        [JsonProperty("uuid")]
        public string TaskId { get; set; }
    }

    [JsonObject("EnvironmentAddedResult")]
    public class EnvironmentAddedResult
    {
        [JsonProperty("result")]
        public EnvironmentAdded Result { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    [JsonObject("V2TaskResultInfo")]
    public class V2TaskResultInfo
    {
        [JsonProperty("deploy_scenario")]
        public string DeployScenario { get; set; }

        [JsonProperty("installer")]
        public string Installer { get; set; }

        [JsonProperty("pod_name")]
        public string PodName { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }
    }

    [JsonObject("V2TaskResult")]
    public class V2TaskResult
    {
        [JsonProperty("criteria")]
        public string Criteria { get; set; }

        [JsonProperty("info")]
        public V2TaskResultInfo Info { get; set; }

        [JsonProperty("task_id")]
        public string TaskId { get; set; }

        [JsonProperty("testcases")]
        public object Testcases { get; set; }
    }


    [JsonObject("V2TaskInfo")]
    public class V2Taskinfo
    {
        [JsonProperty("case_name")]
        public string CaseName { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("environment_id")]
        public string EnvironmentId { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("params")]
        public string Params { get; set; }

        [JsonProperty("project_id")]
        public string ProjectId { get; set; }

        [JsonProperty("result")]
        public object Result { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("suite")]
        public bool? Suite { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("uuid")]
        public string UUID { get; set; }
    }

    [JsonObject("V2TaskInfoResult")]
    public class V2TaskInfoResult
    {
        [JsonProperty("task")]
        public V2Taskinfo Task { get; set; }
    }

    [JsonObject("GetV2TaskInfo")]
    public class GetV2TaskInfo
    {
        [JsonProperty("result")]
        public V2TaskInfoResult Result { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    [JsonObject("V2TaskCreated")]
    public class V2TaskCreated
    {
        [JsonProperty("uuid")]
        public string taskId { get; set; }
    }

    [JsonObject("V2TaskCreatedResult")]
    public class V2TaskCreatedResult
    {
        [JsonProperty("result")]
        public V2TaskCreated Result { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    [JsonObject("V2CreatedTaskResult")]
    public class V2CreatedTaskResult
    {
        [JsonProperty("uuid")]
        public string TaskId { get; set; }
    }

    [JsonObject("V2DeletedTaskResult")]
    public class V2DeletedTaskResult
    {
        [JsonProperty("task")]
        public string TaskId { get; set; }
    }

    [JsonObject("V1V2RunTestCaseResult")]
    public class V1V2RunTestCaseResult
    {
        [JsonProperty("task_id")]
        public string TaskId { get; set; }
    }

    [JsonObject("V2GetTasksResult")]
    public class V2GetTasksResult
    {
        [JsonProperty("tasks")]
        public List<V2Taskinfo> Tasks { get; set; }
    }

    [JsonObject("V2AddedCaseToTaskResult")]
    public class V2AddedCaseToTaskResult
    {
        [JsonProperty("uuid")]
        public string TaskId { get; set; }
    }

    [JsonObject("V2AddedSuiteToTaskResult")]
    public class V2AddedSuiteToTaskResult
    {
        [JsonProperty("uuid")]
        public string TaskId { get; set; }
    }

    [JsonObject("V2RunTaskResult")]
    public class V2RunTaskResult
    {
        [JsonProperty("uuid")]
        public string TaskId { get; set; }
    }

}
