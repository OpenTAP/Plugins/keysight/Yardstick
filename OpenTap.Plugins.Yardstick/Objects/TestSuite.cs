﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Newtonsoft.Json;
using System.Collections.Generic;

namespace OpenTap.Plugins.Yardstick.Objects
{
    [JsonObject("TestSuite")]
    public class TestSuite
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("schema")]
        public string Schema { get; set; }

        [JsonProperty("test_cases")]
        public List<string> TestCases { get; set; }

        [JsonProperty("test_cases_dir")]
        public string TestCasesDir { get; set; }
    }

    public class TestSuiteListResult
    {
        [JsonProperty("testsuites")]
        public List<string> TestSuites { get; set; }
    }

    [JsonObject("TestSuiteList")]
    public class TestSuiteList
    {
        [JsonProperty("result")]
        public TestSuiteListResult TestSuiteListResult { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    public class V2GetTestSuitesResult
    {
        [JsonProperty("testsuites")]
        public List<string> TestSuites { get; set; }
    }

    [JsonObject("V2GetTestSuiteResult")]
    public class V2GetTestSuiteResult
    {
        [JsonProperty("testsuite")]
        public string TestSuiteContent { get; set; }
    }

    [JsonObject("V2CreatedTestSuiteResult")]
    public class V2CreatedTestSuiteResult
    {
        [JsonProperty("suite")]
        public string TestSuiteName { get; set; }
    }

    [JsonObject("V2DeletedTestSuiteResult")]
    public class V2DeletedTestSuiteResult
    {
        [JsonProperty("testsuite")]
        public string TestSuiteName { get; set; }
    }
}
