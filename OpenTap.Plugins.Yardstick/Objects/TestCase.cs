﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Newtonsoft.Json;
using System.Collections.Generic;

namespace OpenTap.Plugins.Yardstick.Objects
{
    [JsonObject("TestCase")]
    public class TestCase
    {
        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("deploy_scenarios")]
        public string DeployScenarios { get; set; }

        [JsonProperty("installer")]
        public string Installer { get; set; }

        public override string ToString()
        {
            return $"TestCase Name=[{Name}] Description=[{Description}]";
        }
    }

    [JsonObject("TestCaseList")]
    public class TestCaseList
    {
        [JsonProperty("result")]
        public List<TestCase> TestCases { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    [JsonObject("V2GetTestCasesResult")]
    public class V2GetTestCasesResult
    {
        [JsonProperty("testcases")]
        public List<TestCase> TestCases { get; set; }
    }

    [JsonObject("V2DeletedTestCaseResult")]
    public class V2DeletedTestCaseResult
    {
        [JsonProperty("testcase")]
        public string testCaseName { get; set; }
    }

    [JsonObject("V2UploadedTestCaseResult")]
    public class V2UploadedTestCaseResult
    {
        [JsonProperty("testcase")]
        public string testCase { get; set; }
    }

    [JsonObject("V2GetTestCaseArg")]
    public class V2GetTestCaseArg
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    [JsonObject("V2GetTestCaseResult")]
    public class V2GetTestCaseResult
    {
        [JsonProperty("args")]
        public object Args { get; set; }
        //public Dictionary<string, V2GetTestCaseArg> Args { get; set; }

        [JsonProperty("testcase")]
        public string TestCaseContent { get; set; }
    }
}
