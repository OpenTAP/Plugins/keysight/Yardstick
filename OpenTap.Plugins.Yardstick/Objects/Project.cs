﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Newtonsoft.Json;
using System.Collections.Generic;

namespace OpenTap.Plugins.Yardstick.Objects
{
    [JsonObject("Project")]
    public class Project
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("tasks")]
        public List<string> TasksIds { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("uuid")]
        public string UUID { get; set; }

        public override string ToString()
        {
            return $"Project Name=[{Name}] Id=[{Id}] Description=[{Description}] UUID=[{UUID}] TasksIds=[{string.Join(",", TasksIds)}]";
        }
    }

    [JsonObject("ProjectList")]
    public class ProjectList
    {
        [JsonProperty("projects")]
        public List<Project> Projects { get; set; }
    }

    [JsonObject("ProjectListResult")]
    public class ProjectListResult
    {
        [JsonProperty("result")]
        public ProjectList ProjectList { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    [JsonObject("ProjectCreatedResult")]
    public class ProjectCreatedResult
    {
        [JsonProperty("result")]
        public Project Project { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    [JsonObject("ProjectResult")]
    public class ProjectResult
    {
        [JsonProperty("project")]
        public Project Project { get; set; }
    }

    [JsonObject("ProjectInfoResult")]
    public class ProjectInfoResult
    {
        [JsonProperty("result")]
        public ProjectResult ProjectResult { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    [JsonObject("ProjectDeleted")]
    public class ProjectDeleted
    {
        [JsonProperty("project")]
        public string ProjectId { get; set; }
    }

    [JsonObject("ProjectDeletedResult")]
    public class ProjectDeletedResult
    {
        [JsonProperty("result")]
        public ProjectDeleted ProjectDeleted { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    [JsonObject("LogResult")]
    public class LogData
    {
        [JsonProperty("data")]
        public List<string> Data { get; set; }

        [JsonProperty("index")]
        public int Index { get; set; }
    }

    [JsonObject("LogResult")]
    public class LogResult
    {
        [JsonProperty("result")]
        public LogData LogData { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    [JsonObject("V2CreatedProjectResult")]
    public class V2CreatedProjectResult
    {
        [JsonProperty("uuid")]
        public string ProjectId { get; set; }
    }

    [JsonObject("V2GetProjectResult")]
    public class V2GetProjectResult
    {
        [JsonProperty("project")]
        public Project Project { get; set; }
    }

    [JsonObject("V2DeletedProjectResult")]
    public class V2DeletedProjectResult
    {
        [JsonProperty("project")]
        public string ProjectId { get; set; }
    }

    [JsonObject("V2GetProjectsResult")]
    public class V2GetProjectsResult
    {
        [JsonProperty("projects")]
        public List<Project> Projects { get; set; }
    }
}
