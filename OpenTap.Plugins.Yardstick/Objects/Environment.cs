﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using Newtonsoft.Json;
using System.Collections.Generic;

namespace OpenTap.Plugins.Yardstick.Objects
{
    public class Environment
    {
        [JsonProperty("container_id")]
        public object ContainerId { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("image_id")]
        public object[] ImageId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("openrc_id")]
        public string OpenrcId { get; set; }

        [JsonProperty("pod_id")]
        public string PodId { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("uuid")]
        public string UUID { get; set; }

        public override string ToString()
        {
            return $"Environment Id:[{Id}] Name:[{Name}] Description:[{Description}]";
        }
    }

    public class EnvironmentList
    {
        [JsonProperty("environments")]
        public List<Environment> Environments { get; set; }
    }

    public class EnvironmentListResult
    {
        [JsonProperty("result")]
        public EnvironmentList Result { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }


    [JsonObject("EnvironmentCreatedResult")]
    public class EnvironmentCreatedResult
    {
        [JsonProperty("result")]
        public Environment Environment { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    public class EnvironmentDeletedResult : EnvironmentCreatedResult
    {
    }

    [JsonObject("Openrc")]
    public class Openrc
    {
        [JsonProperty("OS_AUTH_URL")]
        public string OsAuthUrl { get; set; }

        [JsonProperty("OS_IDENTITY_API_VERSION")]
        public string OsIdentityApiVersion { get; set; }

        [JsonProperty("OS_INTERFACE")]
        public string OsInterface { get; set; }

        [JsonProperty("OS_PASSWORD")]
        public string OsPassword { get; set; }

        [JsonProperty("OS_PROJECT_DOMAIN_ID")]
        public string OsProjectDomainId { get; set; }

        [JsonProperty("OS_PROJECT_ID")]
        public string OsProjectId { get; set; }

        [JsonProperty("OS_PROJECT_NAME")]
        public string OsProjectName { get; set; }

        [JsonProperty("OS_REGION_NAME")]
        public string OsRegionName { get; set; }

        [JsonProperty("OS_USERNAME")]
        public string OsUsername { get; set; }

        [JsonProperty("OS_USER_DOMAIN_NAME")]
        public string OsUserDomainName { get; set; }
    }

    [JsonObject("OpenrcUploadedResult")]
    public class OpenrcUploadedResult
    {
        [JsonProperty("openrc")]
        public Openrc Openrc { get; set; }

        [JsonProperty("uuid")]
        public string Uuid { get; set; }
    }

    [JsonObject("OpenrcUploaded")]
    public class OpenrcUploaded
    {
        [JsonProperty("result")]
        public OpenrcUploadedResult OpenrcUploadedResult { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
    }

    [JsonObject("V2OpenrcUploadedResult")]
    public class V2OpenrcUploadedResult
    {
        [JsonProperty("openrc")]
        public Openrc Openrc { get; set; }

        [JsonProperty("uuid")]
        public string Uuid { get; set; }
    }

    [JsonObject("V2DeletedEnvironmentResult")]
    public class V2DeletedEnvironmentResult
    {
        [JsonProperty("environment")]
        public string EnvironmentId { get; set; }
    }
}
