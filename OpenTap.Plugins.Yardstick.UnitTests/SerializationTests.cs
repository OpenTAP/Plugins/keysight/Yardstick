﻿//Copyright 2019-2020 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

using OpenTap.Plugins.Yardstick.Objects;
using Newtonsoft.Json;
using NUnit.Framework;

namespace OpenTap.Plugins.Yardstick.UnitTests
{
    [TestFixture]
    public class SerializationTests
    {
        [Test]
        public void DeserializeTestCase()
        {
            string serializedTestCase =
                @"{
                  'Description': 'Measure memory read and write bandwidth using lmbench.',
                  'Name': 'opnfv_yardstick_tc012',
                  'deploy_scenarios': 'all',
                  'installer': 'all'
                }";

            TestCase testCase = JsonConvert.DeserializeObject<TestCase>(serializedTestCase);
            Assert.AreEqual("Measure memory read and write bandwidth using lmbench.", testCase.Description);
            Assert.AreEqual("opnfv_yardstick_tc012", testCase.Name);
            Assert.AreEqual("all", testCase.DeployScenarios);
            Assert.AreEqual("all", testCase.Installer);
        }

        [Test]
        public void DeserializeTestCaseList()
        {
            string serializedTestCaseList =
                @"{
                    'result': [
                      {
                        'Description': 'Measure network throughput using pktgen', 
                        'Name': 'opnfv_yardstick_tc001', 
                        'deploy_scenarios': 'all', 
                        'installer': 'all'
                      }, 
                      {
                        'Description': 'measure network latency using ping', 
                        'Name': 'opnfv_yardstick_tc002', 
                        'deploy_scenarios': 'all', 
                        'installer': 'all'
                      }, 
                      {
                        'Description': 'Measure Storage IOPS, throughput and latency using fio.', 
                        'Name': 'opnfv_yardstick_tc005', 
                        'deploy_scenarios': 'all', 
                        'installer': 'all'
                      }
                    ], 
                    'status': 1
                  }";

            TestCaseList testCaseList = JsonConvert.DeserializeObject<TestCaseList>(serializedTestCaseList);

            Assert.IsNotNull(testCaseList);
            Assert.AreEqual(1, testCaseList.Status);
            Assert.IsNotNull(testCaseList.TestCases);
            Assert.AreEqual(3, testCaseList.TestCases.Count);
            Assert.AreEqual("Measure network throughput using pktgen", testCaseList.TestCases[0].Description);
            Assert.AreEqual("opnfv_yardstick_tc001", testCaseList.TestCases[0].Name);
            Assert.AreEqual("all", testCaseList.TestCases[0].DeployScenarios);
            Assert.AreEqual("all", testCaseList.TestCases[0].Installer);
            Assert.AreEqual("measure network latency using ping", testCaseList.TestCases[1].Description);
            Assert.AreEqual("opnfv_yardstick_tc002", testCaseList.TestCases[1].Name);
            Assert.AreEqual("all", testCaseList.TestCases[1].DeployScenarios);
            Assert.AreEqual("all", testCaseList.TestCases[1].Installer);
            Assert.AreEqual("Measure Storage IOPS, throughput and latency using fio.", testCaseList.TestCases[2].Description);
            Assert.AreEqual("opnfv_yardstick_tc005", testCaseList.TestCases[2].Name);
            Assert.AreEqual("all", testCaseList.TestCases[2].DeployScenarios);
            Assert.AreEqual("all", testCaseList.TestCases[2].Installer);
        }

        [Test]
        public void DeserializeProject()
        {
            string serializedProject =
                @"{
                    'description': null,
                    'id': 1,
                    'name': 'project1',
                    'tasks':
                    [
                      'b84fe38a-d2e6-4683-a2db-f9baf124f0d6',
                      '7856bae3-36be-3f3d-6fd3-a74bc74d2e64'
                    ], 
                    'time': 'Wed, 20 Mar 2019 08:27:42 GMT', 
                    'uuid': '6677f943-1d63-48a8-9f7c-d3542ee78797'
                  }";


            Project project = JsonConvert.DeserializeObject<Project>(serializedProject);


            Assert.AreEqual(null, project.Description);
            Assert.AreEqual(1, project.Id);
            Assert.AreEqual("project1", project.Name);
            Assert.IsNotNull(project.TasksIds);
            Assert.IsNotNull("b84fe38a-d2e6-4683-a2db-f9baf124f0d6", project.TasksIds[0]);
            Assert.IsNotNull("7856bae3-36be-3f3d-6fd3-a74bc74d2e64", project.TasksIds[1]);
            Assert.AreEqual("Wed, 20 Mar 2019 08:27:42 GMT", project.Time);
            Assert.AreEqual("6677f943-1d63-48a8-9f7c-d3542ee78797", project.UUID);
        }

        [Test]
        public void DeserializeProjectList()
        {
            string serializedProjectList =
                @"{
                   'result':
                   {
                     'projects':
                     [
                       {
                         'description': null,
                         'id': 1,
                         'name': 'project1',
                         'tasks':
                         [
                           'b84fe38a-d2e6-4683-a2db-f9baf124f0d6'
                         ], 
                         'time': 'Wed, 20 Mar 2019 08:27:42 GMT', 
                         'uuid': '6677f943-1d63-48a8-9f7c-d3542ee78797'
                       }
                     ]
                   }, 
                   'status': 1
                 }      
                 ";

            ProjectListResult plr = JsonConvert.DeserializeObject<ProjectListResult>(serializedProjectList);

            Assert.IsNotNull(plr);
            Assert.AreEqual(1, plr.Status);
            Assert.IsNotNull(plr.ProjectList);
            Assert.IsNotNull(plr.ProjectList.Projects);
            Assert.AreEqual(1, plr.ProjectList.Projects.Count);

            Assert.AreEqual(null, plr.ProjectList.Projects[0].Description);
            Assert.AreEqual(1, plr.ProjectList.Projects[0].Id);
            Assert.AreEqual("project1", plr.ProjectList.Projects[0].Name);
            Assert.IsNotNull(plr.ProjectList.Projects[0].TasksIds);
            Assert.IsNotNull("b84fe38a-d2e6-4683-a2db-f9baf124f0d6", plr.ProjectList.Projects[0].TasksIds[0]);
            Assert.AreEqual("Wed, 20 Mar 2019 08:27:42 GMT", plr.ProjectList.Projects[0].Time);
            Assert.AreEqual("6677f943-1d63-48a8-9f7c-d3542ee78797", plr.ProjectList.Projects[0].UUID);
        }

        [Test]
        public void DeserializeProjectCreatedResult()
        {
            string serializedProjectCreatedResult =
                   @"{
                       'result':
                       {
                         'uuid': '6677f943-1d63-48a8-9f7c-d3542ee78797'
                       },
                       'status': 1
                     }";

            ProjectCreatedResult pcr = JsonConvert.DeserializeObject<ProjectCreatedResult>(serializedProjectCreatedResult);

            Assert.IsNotNull(pcr);
            Assert.AreEqual(1, pcr.Status);
            Assert.IsNotNull(pcr.Project);
            Assert.AreEqual(null, pcr.Project.Description);
            Assert.AreEqual(0, pcr.Project.Id);
            Assert.AreEqual(null, pcr.Project.Name);
            Assert.AreEqual(null, pcr.Project.TasksIds);
            Assert.AreEqual(null, pcr.Project.Time);
            Assert.AreEqual("6677f943-1d63-48a8-9f7c-d3542ee78797", pcr.Project.UUID);
        }

        [Test]
        public void DeserializeProjectInfoResult()
        {
            string serializedProjectInfoResult =
                   @"{
                         'result':
                         {
                             'project':
                             {
                                 'description': null,
                                 'id': 1,
                                 'name': 'project1',
                                 'tasks': [],
                                 'time': 'Wed, 20 Mar 2019 08:22:24 GMT',
                                 'uuid': '92da969c-5ff5-4a4b-8077-7dfc72655968'
                             }
                         },
                         'status': 1
                     }";

            ProjectInfoResult pir = JsonConvert.DeserializeObject<ProjectInfoResult>(serializedProjectInfoResult);

            Assert.IsNotNull(pir);
            Assert.AreEqual(1, pir.Status);
            Assert.IsNotNull(pir.ProjectResult);
            Assert.IsNotNull(pir.ProjectResult.Project);
            Assert.AreEqual(null, pir.ProjectResult.Project.Description);
            Assert.AreEqual(1, pir.ProjectResult.Project.Id);
            Assert.AreEqual("project1", pir.ProjectResult.Project.Name);
            Assert.IsNotNull(pir.ProjectResult.Project.TasksIds);
            Assert.AreEqual("Wed, 20 Mar 2019 08:22:24 GMT", pir.ProjectResult.Project.Time);
            Assert.AreEqual("92da969c-5ff5-4a4b-8077-7dfc72655968", pir.ProjectResult.Project.UUID);
        }

        [Test]
        public void DeserializeProjectDeletedResult()
        {
            string serializedProjectDeletedResult =
                @"{
                    'result':
                    {
                        'project': '1ad18224-5fd8-47f1-9520-6be29e1e8f42'
                    },
                    'status': 1
                }";

            ProjectDeletedResult pdr = JsonConvert.DeserializeObject<ProjectDeletedResult>(serializedProjectDeletedResult);

            Assert.IsNotNull(pdr);
            Assert.AreEqual(1, pdr.Status);
            Assert.IsNotNull(pdr.ProjectDeleted);
            Assert.AreEqual("1ad18224-5fd8-47f1-9520-6be29e1e8f42", pdr.ProjectDeleted.ProjectId);
        }

        [Test]
        public void DeserializeLogResultWithInvalidContent()
        {
            string serializedLogResult =
                @"{
                    'result': {
                        'data': [
                            '2019-03-20 17:42:56,026 yardstick.benchmark.runners.duration duration.py:65 DEBUG runner=24005 seq=1 START\n',
                            '2019-03-20 17:42:56,027 yardstick.benchmark.scenarios.networking.ping ping.py:75 DEBUG ping -s 100 10.0.1.4\n',
                            '2019-03-20 17:42:58,102 yardstick.ssh.athena ssh.py:300 DEBUG stdout: u'2.196'\n',
                            '2019-03-20 17:43:09,125 yardstick.benchmark.runners.duration duration.py:105 DEBUG runner=24005 seq=1 END\n',
                            '2019-03-20 17:43:09,133 yardstick.benchmark.runners.duration duration.py:65 DEBUG runner=24005 seq=2 START\n',
                            '2019-03-20 17:43:09,133 yardstick.benchmark.scenarios.networking.ping ping.py:75 DEBUG ping -s 100 10.0.1.4\n',
                            '2019-03-20 17:43:09,811 yardstick.benchmark.runners.base base.py:258 DEBUG result_queue size 1\n',
                            '2019-03-20 17:43:11,144 yardstick.ssh.athena ssh.py:300 DEBUG stdout: u'1.236'\n',
                            '2019-03-20 17:43:22,159 yardstick.benchmark.runners.duration duration.py:105 DEBUG runner=24005 seq=2 END\n',
                            '2019-03-20 17:43:22,159 yardstick.benchmark.runners.duration duration.py:65 DEBUG runner=24005 seq=3 START\n',
                            '2019-03-20 17:43:22,160 yardstick.benchmark.scenarios.networking.ping ping.py:75 DEBUG ping -s 100 10.0.1.4\n',
                            '2019-03-20 17:43:24,171 yardstick.ssh.athena ssh.py:300 DEBUG stdout: u'1.057'\n',
                            '2019-03-20 17:43:24,813 yardstick.benchmark.runners.base base.py:258 DEBUG result_queue size 1\n',
                            '2019-03-20 17:43:35,184 yardstick.benchmark.runners.duration duration.py:105 DEBUG runner=24005 seq=3 END\n',
                            '2019-03-20 17:43:35,185 yardstick.benchmark.runners.duration duration.py:65 DEBUG runner=24005 seq=4 START\n',
                            '2019-03-20 17:43:35,185 yardstick.benchmark.scenarios.networking.ping ping.py:75 DEBUG ping -s 100 10.0.1.4\n',
                            '2019-03-20 17:43:37,199 yardstick.ssh.athena ssh.py:300 DEBUG stdout: u'1.353'\n',
                            '2019-03-20 17:43:39,815 yardstick.benchmark.runners.base base.py:258 DEBUG result_queue size 1\n',
                            '2019-03-20 17:43:48,213 yardstick.benchmark.runners.duration duration.py:105 DEBUG runner=24005 seq=4 END\n',
                            '2019-03-20 17:43:48,221 yardstick.benchmark.runners.duration duration.py:65 DEBUG runner=24005 seq=5 START\n',
                            '2019-03-20 17:43:48,221 yardstick.benchmark.scenarios.networking.ping ping.py:75 DEBUG ping -s 100 10.0.1.4\n',
                            '2019-03-20 17:43:49,818 yardstick.benchmark.runners.base base.py:258 DEBUG result_queue size 1\n',
                            '2019-03-20 17:43:50,232 yardstick.ssh.athena ssh.py:300 DEBUG stdout: u'1.170'\n',
                            '2019-03-20 17:44:01,246 yardstick.benchmark.runners.duration duration.py:105 DEBUG runner=24005 seq=5 END\n',
                            '2019-03-20 17:44:01,254 yardstick.benchmark.runners.duration duration.py:111 INFO Worker END\n',
                            '2019-03-20 17:44:01,255 yardstick.benchmark.runners.duration duration.py:124 DEBUG queue.qsize() = 1\n',
                            '2019-03-20 17:44:01,255 yardstick.benchmark.runners.duration duration.py:125 DEBUG output_queue.qsize() = 0\n',
                            '2019-03-20 17:44:01,287 yardstick.benchmark.runners.base base.py:258 DEBUG result_queue size 1\n',
                            '2019-03-20 17:44:01,288 yardstick.benchmark.core.task task.py:284 INFO Runner ended\n',
                            '2019-03-20 17:44:01,289 yardstick.benchmark.core.task task.py:377 DEBUG Got a KeyError in config_context_target({})\n',
                            '2019-03-20 17:44:01,292 yardstick.benchmark.core.task task.py:404 INFO Starting runner of type 'Duration'\n',
                            '2019-03-20 17:44:01,302 yardstick.benchmark.runners.duration duration.py:47 INFO Worker START, duration is 60s\n',
                            '2019-03-20 17:44:01,302 yardstick.benchmark.runners.duration duration.py:48 DEBUG class is <class 'yardstick.benchmark.scenarios.networking.ping.Ping'>\n',
                            '2019-03-20 17:44:01,307 yardstick.ssh.athena ssh.py:156 DEBUG user:cirros host:192.168.11.34\n',
                            '2019-03-20 17:44:01,454 yardstick.ssh.athena ssh.py:300 DEBUG stdout: u'Linux\\n'\n',
                            '2019-03-20 17:44:01,462 yardstick.benchmark.runners.duration duration.py:65 DEBUG runner=24029 seq=1 START\n',
                            '2019-03-20 17:44:01,462 yardstick.benchmark.scenarios.networking.ping ping.py:75 DEBUG ping -s 100 10.0.1.4\n',
                            '2019-03-20 17:44:03,534 yardstick.ssh.athena ssh.py:300 DEBUG stdout: u'1.223'\n',
                            '2019-03-20 17:44:14,554 yardstick.benchmark.runners.duration duration.py:105 DEBUG runner=24029 seq=1 END\n',
                            '2019-03-20 17:44:14,554 yardstick.benchmark.runners.duration duration.py:65 DEBUG runner=24029 seq=2 START\n',
                            '2019-03-20 17:44:14,555 yardstick.benchmark.scenarios.networking.ping ping.py:75 DEBUG ping -s 100 10.0.1.4\n',
                            '2019-03-20 17:44:16,298 yardstick.benchmark.runners.base base.py:258 DEBUG result_queue size 1\n',
                            '2019-03-20 17:44:16,566 yardstick.ssh.athena ssh.py:300 DEBUG stdout: u'1.308'\n',
                            '2019-03-20 17:44:27,581 yardstick.benchmark.runners.duration duration.py:105 DEBUG runner=24029 seq=2 END\n',
                            '2019-03-20 17:44:27,588 yardstick.benchmark.runners.duration duration.py:65 DEBUG runner=24029 seq=3 START\n',
                            '2019-03-20 17:44:27,588 yardstick.benchmark.scenarios.networking.ping ping.py:75 DEBUG ping -s 100 10.0.1.4\n',
                            '2019-03-20 17:44:29,601 yardstick.ssh.athena ssh.py:300 DEBUG stdout: u'1.335'\n',
                            '2019-03-20 17:44:31,301 yardstick.benchmark.runners.base base.py:258 DEBUG result_queue size 1\n',
                            '2019-03-20 17:44:40,621 yardstick.benchmark.runners.duration duration.py:105 DEBUG runner=24029 seq=3 END\n',
                            '2019-03-20 17:44:40,628 yardstick.benchmark.runners.duration duration.py:65 DEBUG runner=24029 seq=4 START\n',
                            '2019-03-20 17:44:40,628 yardstick.benchmark.scenarios.networking.ping ping.py:75 DEBUG ping -s 100 10.0.1.4\n',
                            '2019-03-20 17:44:41,302 yardstick.benchmark.runners.base base.py:258 DEBUG result_queue size 1\n',
                            '2019-03-20 17:44:41,637 api.resources.v1.results results.py:65 DEBUG Task status is: 1\n',
                            '2019-03-20 17:44:42,641 yardstick.ssh.athena ssh.py:300 DEBUG stdout: u'1.259'\n',
                            '2019-03-20 17:44:52,185 api.resources.v1.results results.py:65 DEBUG Task status is: 0\n',
                            '2019-03-20 17:44:53,657 yardstick.benchmark.runners.duration duration.py:105 DEBUG runner=24029 seq=4 END\n',
                            '2019-03-20 17:44:53,666 yardstick.benchmark.runners.duration duration.py:65 DEBUG runner=24029 seq=5 START\n',
                            '2019-03-20 17:44:53,666 yardstick.benchmark.scenarios.networking.ping ping.py:75 DEBUG ping -s 100 10.0.1.4\n',
                            '2019-03-20 17:44:55,678 yardstick.ssh.athena ssh.py:300 DEBUG stdout: u'1.265'\n',
                            '2019-03-20 17:44:56,304 yardstick.benchmark.runners.base base.py:258 DEBUG result_queue size 1\n',
                            '2019-03-20 17:44:59,393 api.resources.v1.results results.py:65 DEBUG Task status is: 1\n',
                            '2019-03-20 17:45:06,693 yardstick.benchmark.runners.duration duration.py:105 DEBUG runner=24029 seq=5 END\n',
                            '2019-03-20 17:45:06,694 yardstick.benchmark.runners.duration duration.py:111 INFO Worker END\n',
                            '2019-03-20 17:45:06,694 yardstick.benchmark.runners.duration duration.py:124 DEBUG queue.qsize() = 1\n',
                            '2019-03-20 17:45:06,695 yardstick.benchmark.runners.duration duration.py:125 DEBUG output_queue.qsize() = 0\n',
                            '2019-03-20 17:45:06,721 yardstick.benchmark.runners.base base.py:258 DEBUG result_queue size 1\n',
                            '2019-03-20 17:45:06,722 yardstick.benchmark.core.task task.py:284 INFO Runner ended\n',
                            '2019-03-20 17:45:06,723 yardstick.benchmark.core.task task.py:128 INFO Testcase: \'opnfv_yardstick_tc002\' SUCCESS!!!\n',
                            '2019-03-20 17:45:06,724 yardstick.benchmark.contexts.heat heat.py:459 INFO Undeploying context 'demo-736cc70c' START\n',
                            '2019-03-20 17:45:19,741 api.resources.v1.results results.py:65 DEBUG Task status is: 0\n',
                            '2019-03-20 17:45:23,728 api.resources.v1.results results.py:65 DEBUG Task status is: 0\n',
                            '2019-03-20 17:45:29,719 api.resources.v1.results results.py:65 DEBUG Task status is: 1\n',
                            '2019-03-20 17:45:32,615 yardstick.benchmark.contexts.heat heat.py:462 INFO Undeploying context 'demo-736cc70c' DONE\n',
                            '2019-03-20 17:45:32,624 yardstick.benchmark.core.task task.py:147 INFO Task /home/opnfv/repos/yardstick/tests/opnfv/test_cases/opnfv_yardstick_tc002.yaml finished in 196 secs\n',
                            '2019-03-20 17:45:32,638 yardstick.benchmark.core.task task.py:168 INFO Report can be found in '/tmp/report.html'\n',
                            '2019-03-20 17:45:32,638 yardstick.benchmark.core.task task.py:156 INFO Total finished in 196 secs\n',
                            '2019-03-20 17:45:32,638 yardstick.benchmark.core.task task.py:159 INFO To generate report, execute command \'yardstick report generate 736cc70c-e857-4085-86fe-e5789c9dd709 <YAML_NAME>\'\n',
                            '2019-03-20 17:45:32,639 yardstick.benchmark.core.task task.py:160 INFO Task ALL DONE, exiting\n',
                            '2019-03-20 17:45:32,639 api.utils.thread thread.py:44 INFO Task Finished\n',
                            '2019-03-20 17:45:32,640 api.utils.thread thread.py:45 DEBUG Result: {'status': 1, 'result': {'info': {'installer': 'unknown', 'version': 'unknown', 'deploy_scenario': 'unknown', 'pod_name': 'unknown'}, 'testcases': {'opnfv_yardstick_tc002': {'tc_data': [{'timestamp': 1553103789.124366, 'errors': '', 'data': {'rtt.ares': 2.196}, 'sequence': 1}, {'timestamp': 1553103802.159275, 'errors': '', 'data': {'rtt.ares': 1.236}, 'sequence': 2}, {'timestamp': 1553103815.183576, 'errors': '', 'data': {'rtt.ares': 1.057}, 'sequence': 3}, {'timestamp': 1553103828.213667, 'errors': '', 'data': {'rtt.ares': 1.353}, 'sequence': 4}, {'timestamp': 1553103841.24624, 'errors': '', 'data': {'rtt.ares': 1.17}, 'sequence': 5}, {'timestamp': 1553103854.550082, 'errors': '', 'data': {'rtt.ares': 1.223}, 'sequence': 1}, {'timestamp': 1553103867.580727, 'errors': '', 'data': {'rtt.ares': 1.308}, 'sequence': 2}, {'timestamp': 1553103880.620904, 'errors': '', 'data': {'rtt.ares': 1.335}, 'sequence': 3}, {'timestamp': 1553103893.657389, 'errors': '', 'data': {'rtt.ares': 1.259}, 'sequence': 4}, {'timestamp': 1553103906.69318, 'errors': '', 'data': {'rtt.ares': 1.265}, 'sequence': 5}], 'criteria': 'PASS'}}, 'task_id': '736cc70c-e857-4085-86fe-e5789c9dd709', 'criteria': 'PASS'}}\n'
                        ],
                    'index': 18010
                    },
                   'status': 1
                }";

            bool exceptionCaught = false;
            try
            {
                LogResult lr = JsonConvert.DeserializeObject<LogResult>(serializedLogResult);
            }
            catch(Newtonsoft.Json.JsonReaderException ex)
            {
                exceptionCaught = true;
                Assert.AreEqual(6, ex.LineNumber);
                Assert.AreEqual(101, ex.LinePosition);
            }

            Assert.IsTrue(exceptionCaught);
        }
        
        [Test]
        public void DeserializeLogResult()
        {
            string serializedLogResult =
                @"{
                    'result': {
                        'data': [
                            '2019-03-20 17:42:15,867 yardstick.benchmark.contexts.heat heat.py:114 WARNING No pod file specified. NVFi metrics will be disabled\n',
                            '2019-03-20 17:42:15,868 yardstick.benchmark.contexts.heat heat.py:336 INFO Deploying context \'demo-736cc70c\' START\n',
                            '2019-03-20 17:42:15,883 yardstick.orchestrator.heat heat.py:177 DEBUG template object \'demo-736cc70c\' created\n',
                            '2019-03-20 17:42:15,883 yardstick.orchestrator.heat heat.py:440 DEBUG adding Nova::KeyPair \'demo-736cc70c-key\'\n',
                            '2019-03-20 17:42:15,883 yardstick.orchestrator.heat heat.py:476 DEBUG adding Neutron::SecurityGroup \'demo-736cc70c-secgroup\'\n',
                            '2019-03-20 17:42:15,884 yardstick.orchestrator.heat heat.py:246 DEBUG adding Neutron::Net \'demo-736cc70c-test\'\n',
                            '2019-03-20 17:42:15,884 yardstick.orchestrator.heat heat.py:286 DEBUG adding Neutron::Subnet \'demo-736cc70c-test-subnet\' in network \'demo-736cc70c-test\', cidr \'10.0.1.0/24\'\n',
                            '2019-03-20 17:42:15,884 yardstick.orchestrator.heat heat.py:317 DEBUG adding Neutron::Router:\'demo-736cc70c-test-router\', gw-net:\'public\'\n',
                            '2019-03-20 17:42:15,885 yardstick.orchestrator.heat heat.py:332 DEBUG adding Neutron::RouterInterface \'demo-736cc70c-test-router-if0\' router:\'demo-736cc70c-test-router\', subnet:\'demo-736cc70c-test-subnet\'\n',
                            '2019-03-20 17:42:15,886 yardstick.orchestrator.heat heat.py:405 DEBUG adding Nova::FloatingIP \'athena.demo-736cc70c-fip\', network \'public\', port \'athena.demo-736cc70c-test-port\', rif \'demo-736cc70c-test-router-if0\'\n',
                            '2019-03-20 17:42:15,886 yardstick.orchestrator.heat heat.py:427 DEBUG adding Nova::FloatingIPAssociation \'athena.demo-736cc70c-fip-assoc\', server \'athena.demo-736cc70c-test-port\', floating_ip \'athena.demo-736cc70c-fip\'\n',
                            '2019-03-20 17:42:15,886 yardstick.orchestrator.heat heat.py:630 INFO Creating stack \'demo-736cc70c\' START\n',
                            '2019-03-20 17:42:49,111 yardstick.orchestrator.heat heat.py:648 INFO Creating stack \'demo-736cc70c\' DONE in 33 secs\n',
                            '2019-03-20 17:42:49,798 yardstick.benchmark.contexts.heat heat.py:377 INFO Deploying context \'demo-736cc70c\' DONE\n',
                            '2019-03-20 17:42:49,802 yardstick.benchmark.core.task task.py:404 INFO Starting runner of type \'Duration\'\n',
                            '2019-03-20 17:42:49,815 yardstick.benchmark.runners.duration duration.py:47 INFO Worker START, duration is 60s\n',
                            '2019-03-20 17:42:49,816 yardstick.benchmark.runners.duration duration.py:48 DEBUG class is <class \'yardstick.benchmark.scenarios.networking.ping.Ping\'>\n',
                            '2019-03-20 17:42:49,819 yardstick.ssh.athena ssh.py:156 DEBUG user:cirros host:192.168.11.34\n'
                        ],
                    'index': 18010
                    },
                   'status': 1
                }";

            LogResult lr = JsonConvert.DeserializeObject<LogResult>(serializedLogResult);

            Assert.IsNotNull(lr);
            Assert.AreEqual(1, lr.Status);
            Assert.IsNotNull(lr.LogData);
            Assert.AreEqual(18010, lr.LogData.Index);
            Assert.IsNotNull(lr.LogData.Data);
            Assert.AreEqual(18, lr.LogData.Data.Count);
        }
        
        [Test]
        public void DeserializeTaskStatus()
        {
            string serializedTaskStatus =
                @"{
                    'result': {
                        'criteria': 'PASS',
                        'info': {
                            'deploy_scenario': 'unknown',
                            'installer': 'unknown',
                            'pod_name': 'unknown',
                            'version': 'unknown'
                        },
                        'task_id': '5155e800-2378-4e91-91e3-f05573298b67',
                        'testcases': {
                            'opnfv_yardstick_tc002': {
                                'criteria': 'PASS',
                                'tc_data': [
                                    {
                                        'data': {
                                            'rtt.ares': 2.497
                                        },
                                        'errors': '',
                                        'sequence': 1,
                                        'timestamp': 1553095486.323462
                                    },
                                    {
                                        'data': {
                                            'rtt.ares': 1.339
                                        },
                                        'errors': '',
                                        'sequence': 2,
                                        'timestamp': 1553095499.350285
                                    },
                                    {
                                        'data': {
                                            'rtt.ares': 6.434
                                        },
                                        'errors': '',
                                        'sequence': 3,
                                        'timestamp': 1553095512.384629
                                    },
                                    {
                                        'data': {
                                            'rtt.ares': 1.318
                                        },
                                        'errors': '',
                                        'sequence': 4,
                                        'timestamp': 1553095525.403783
                                    },
                                    {
                                        'data': {
                                            'rtt.ares': 1.394
                                        },
                                        'errors': '',
                                        'sequence': 5,
                                        'timestamp': 1553095538.431456
                                    },
                                    {
                                        'data': {
                                            'rtt.ares': 1.298
                                        },
                                        'errors': '',
                                        'sequence': 1,
                                        'timestamp': 1553095551.757861
                                    },
                                    {
                                        'data': {
                                            'rtt.ares': 1.364
                                        },
                                        'errors': '',
                                        'sequence': 2,
                                        'timestamp': 1553095564.784984
                                    },
                                    {
                                        'data': {
                                            'rtt.ares': 1.369
                                        },
                                        'errors': '',
                                        'sequence': 3,
                                        'timestamp': 1553095577.815637
                                    },
                                    {
                                        'data': {
                                            'rtt.ares': 1.285
                                        },
                                        'errors': '',
                                        'sequence': 4,
                                        'timestamp': 1553095590.846185
                                    },
                                    {
                                        'data': {
                                            'rtt.ares': 1.351
                                        },
                                        'errors': '',
                                        'sequence': 5,
                                        'timestamp': 1553095603.87423
                                    }
                                ]
                            }
                        }
                    },
                    'status': 1
                }";

            TaskStatus ts = JsonConvert.DeserializeObject<TaskStatus>(serializedTaskStatus);

            Assert.IsNotNull(ts);
            Assert.AreEqual(1, ts.Status);
            Assert.IsNotNull(ts.Result);
            Assert.AreEqual("PASS", ts.Result.Criteria);
            Assert.AreEqual("5155e800-2378-4e91-91e3-f05573298b67", ts.Result.TaskId);
            Assert.IsNotNull(ts.Result.Info);
            Assert.AreEqual("unknown", ts.Result.Info.DeployScenario);
            Assert.AreEqual("unknown", ts.Result.Info.Installer);
            Assert.AreEqual("unknown", ts.Result.Info.PodName);
            Assert.AreEqual("unknown", ts.Result.Info.Version);
        }

        [Test]
        public void DeserializeEnvironmentList()
        {
            string serializedEnvironmentList =
                @"{
                    'result': {
                        'environments': [
                            {
                                'container_id': {},
                                'description': null,
                                'id': 1,
                                'image_id': [],
                                'name': 'task122',
                                'openrc_id': null,
                                'pod_id': null,
                                'time': null,
                                'uuid': '7041fbec-5d60-471d-bb8a-d5ac7e037eaa'
                            }
                        ]
                    },
                    'status': 1
                }";

            EnvironmentListResult elr = JsonConvert.DeserializeObject<EnvironmentListResult>(serializedEnvironmentList);

            Assert.IsNotNull(elr);
            Assert.AreEqual(1, elr.Status);
            Assert.IsNotNull(elr.Result);
            Assert.IsNotNull(elr.Result.Environments);
            Assert.AreEqual(1, elr.Result.Environments.Count);
            Assert.IsNotNull(elr.Result.Environments[0].ContainerId);
            Assert.AreEqual(null, elr.Result.Environments[0].Description);
            Assert.AreEqual(1, elr.Result.Environments[0].Id);
            Assert.AreEqual("task122", elr.Result.Environments[0].Name);
            Assert.AreEqual(null, elr.Result.Environments[0].OpenrcId);
            Assert.AreEqual(null, elr.Result.Environments[0].PodId);
            Assert.AreEqual(null, elr.Result.Environments[0].Time);
            Assert.AreEqual("7041fbec-5d60-471d-bb8a-d5ac7e037eaa", elr.Result.Environments[0].UUID);
        }
    }
}
